#include <iostream>
using std::cout;
using std::endl;
#include "TRandom.h"
#include "TH1D.h"
#include "TFile.h"
#include "TVectorD.h"
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TRandom.h"
#include "TPostScript.h"
#include "TH2D.h"
#include "TFile.h"
#include "TLine.h"
#include "TNtuple.h"
#include "TProfile.h"
#include "TH3D.h"
#include "MacroAnalysisJPsi/FitJPsi.C"


enum {kCoh, kIncoh};
const Char_t *label[2] = {"Coh","Incoh"};

//Version pour #Delta#phi
const Int_t nBinPt = 2;
const Int_t nBinDeltaPhi = 12;

//Version pour spectrum pT
//const Int_t nBinPt = 20;
//const Int_t nBinDeltaPhi = 1;

const Int_t nBinMass = 60;

//Version pour #Delta#Phi
Double_t minPt = 0.0,  maxPt = 1.0;

//Version pour spectrum pT
//Double_t minPt = 0.0, maxPt = 1.5;

Double_t minDeltaPhi = -TMath::Pi(), maxDeltaPhi = TMath::Pi();
Double_t minMass = 2.5, maxMass = 4.0;

//Version pour #Delta#phi
Double_t binPtValueRec[nBinPt+1]= {0, 0.25, maxPt};
Double_t binDeltaPhiValueRec[nBinDeltaPhi+1] = {0};

//Version pour spectrum pT
//Double_t binPtValueRec[nBinPt+1] = {0}; 
//= {0, 0.075, 0.125, 0.185 , 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 1., 1.25, 1.50, 2., 2.5};
//Double_t binDeltaPhiValueRec[nBinDeltaPhi+1] = {minDeltaPhi,maxDeltaPhi};

Double_t binMasseValueRec[nBinMass+1] = {0};
Double_t parPt=1.;

TH3D *hMassPtDeltaPhiCoh = 0;
TH1D *hMassProjCoh[nBinPt+1][nBinDeltaPhi+1]={{0}};
TH3D *hMassPtDeltaPhiIncoh = 0;
TH1D *hMassProjIncoh[nBinPt+1][nBinDeltaPhi+1]={{0}};
TH1D *hMassTotCoh = 0;
TH1D *hMassTotIncoh = 0;
TFile *fileJPsi = 0, *filePsi2s = 0;
TF1 *fitJPsi = 0, *fitPsi2s = 0;
//====================================================================================================================================================

void GetProjectFitData(const Char_t *fData="AnalysisResultsData_q_r.root"){
  //const Char_t *fFit="MacroAnalysisJPsi/FitMC/FitDataPt1.00DeltaPhi2.00.root"){

  //Effacer apres
  //   TFile *fileFit = new TFile(fFit);
  // Reading input file

  Double_t ptRec,deltaPhiRec,masseRec,ptSingleMuon1, ptSingleMuon2;
  
  TFile *fileData = new TFile(fData);
  TTree *tree = (TTree*)fileData->Get("MyTask/fTree");
  auto nentriesData = tree->GetEntries();
  tree->SetBranchAddress("fMuMuPt", &ptRec);
  tree->SetBranchAddress("fMuMuDeltaPhi", &deltaPhiRec);
  tree->SetBranchAddress("fMuMuM",&masseRec);
  tree -> SetBranchAddress("fMuPt1", &ptSingleMuon1);
  tree -> SetBranchAddress("fMuPt2", &ptSingleMuon2);
 
  //Booking instogram   

  //Decommenter pour #Delta#phi
  for(int i = 0;i<nBinDeltaPhi+1;i++){
    Double_t step = (maxDeltaPhi-minDeltaPhi)/nBinDeltaPhi;
    binDeltaPhiValueRec[i] = {minDeltaPhi+i*step};
  }
  for(int i = 0;i<nBinMass+1;i++){
    Double_t step = (maxMass-minMass)/nBinMass;
    binMasseValueRec[i] = {minMass+step*i};
  }
  //Decommenter pour pT
  // for(int i = 0;i<nBinPt+1;i++){
  //   Double_t step = (maxPt-minPt)/nBinPt;
  //   binPtValueRec[i] = {minPt+step*i};
  // }

  hMassPtDeltaPhiCoh = new TH3D("hMassPtDeltaPhiCoh","hMassPtDeltaPhiCoh",nBinMass,binMasseValueRec,nBinPt,binPtValueRec,nBinDeltaPhi,binDeltaPhiValueRec);
  hMassPtDeltaPhiCoh -> SetXTitle("Mass (GeV/c^{2})");
  hMassPtDeltaPhiCoh -> SetYTitle("p_{T} (GeV/c");
  hMassPtDeltaPhiCoh -> SetZTitle("#Delta#phi");
  hMassPtDeltaPhiCoh -> Sumw2(); 

  hMassPtDeltaPhiIncoh = new TH3D("hMassPtDeltaPhiIncoh","hMassPtDeltaPhiIncoh",nBinMass,binMasseValueRec,nBinPt,binPtValueRec,nBinDeltaPhi,binDeltaPhiValueRec);
  hMassPtDeltaPhiIncoh -> SetXTitle("Mass (GeV/c^{2})");
  hMassPtDeltaPhiIncoh -> SetYTitle("p_{T} (GeV/c");
  hMassPtDeltaPhiIncoh -> SetZTitle("#Delta#phi");
  hMassPtDeltaPhiIncoh -> Sumw2(); 

  hMassTotCoh = new TH1D("hMassTotCoh","hMassTotCoh",nBinMass,binMasseValueRec);
  hMassTotCoh -> SetXTitle("Mass (GeV/c^{2})");
  hMassTotCoh -> Sumw2();

  hMassTotIncoh = new TH1D("hMassTotIncoh","hMassTotIncoh",nBinMass,binMasseValueRec);
  hMassTotIncoh -> SetXTitle("Mass (GeV/c^{2})");
  hMassTotIncoh -> Sumw2();

  //Filling 3D histo
 
  for(int iEntry = 0; iEntry<nentriesData; iEntry++){
    tree->GetEntry(iEntry);
    if(ptSingleMuon2 >parPt && ptSingleMuon1 >parPt){
      if(ptRec < 0.25){
	hMassPtDeltaPhiCoh->Fill(masseRec,ptRec,deltaPhiRec);
	hMassTotCoh->Fill(masseRec);
      }
      if(ptRec >= 0.25){
	hMassPtDeltaPhiIncoh->Fill(masseRec,ptRec,deltaPhiRec); 
	hMassTotIncoh->Fill(masseRec);
      }
    }
  }
 for(Int_t i = 0;i<nBinPt;i++){
    for(Int_t j = 0;j<nBinDeltaPhi;j++){
      hMassProjCoh[i][j] = hMassPtDeltaPhiCoh->ProjectionX(Form("hMassPtDeltaPhiCoh%d-%d",i+1,j+1),i,i+1,j,j+1);
      hMassProjCoh[i][j] -> SetTitle(Form("Invariant Mass Coh ( Pt(%.2f-%.2f) #Delta#Phi(%.2f-%.2f) )",binPtValueRec[i],binPtValueRec[i+1],binDeltaPhiValueRec[j],binDeltaPhiValueRec[j+1]));
      hMassProjIncoh[i][j] = hMassPtDeltaPhiIncoh->ProjectionX(Form("hMassPtDeltaPhiIncoh%d-%d",i+1,j+1),i,i+1,j,j+1);
      hMassProjIncoh[i][j] -> SetTitle(Form("Invariant Mass Coh ( Pt(%.2f-%.2f) #Delta#Phi(%.2f-%.2f) )",binPtValueRec[i],binPtValueRec[i+1],binDeltaPhiValueRec[j],binDeltaPhiValueRec[j+1]));
      if(binPtValueRec[i]<0.25){
 	fileJPsi = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCJPsi_0_Pt%.2fDeltaPhi%.2f.root",binPtValueRec[i],binDeltaPhiValueRec[j]));
 		filePsi2s = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCPsi2s_0_Pt%.2fDeltaPhi%.2f.root",binPtValueRec[i],binDeltaPhiValueRec[j]));
 	TH1D *hTmpJPsi = 0, *hTmpPsi2s = 0;
 	hTmpJPsi = (TH1D*) fileJPsi->Get(Form("hMassPtDeltaPhi_%d-%d",i+1,j+1));
 	hTmpJPsi -> SetName("hJPsi");
 	hTmpPsi2s = (TH1D*) filePsi2s->Get(Form("hMassPtDeltaPhi_%d-%d",i+1,j+1));
 	hTmpPsi2s -> SetName("hPsi2s");
 	fitJPsi = hTmpJPsi->GetFunction("fitJPsi");
 	fitJPsi -> SetLineColor(kBlack);
 	fitPsi2s = hTmpPsi2s->GetFunction("fitPsi2s");
 	FitJPsi(hMassProjCoh[i][j],kFALSE,2.4,4.,binPtValueRec[i],binDeltaPhiValueRec[j],-1,-1,-1,fitJPsi,fitPsi2s);
      }
 	else{
     	fileJPsi = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCJPsi_1_Pt%.2fDeltaPhi%.2f.root",binPtValueRec[i],binDeltaPhiValueRec[j]));
     	filePsi2s = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCPsi2s_1_Pt%.2fDeltaPhi%.2f.root",binPtValueRec[i],binDeltaPhiValueRec[j]));
     	TH1D *hTmpJPsi = 0, *hTmpPsi2s = 0;
     	hTmpJPsi = (TH1D*) fileJPsi->Get(Form("hMassPtDeltaPhi_%d-%d",i+1,j+1));
     	hTmpJPsi -> SetName("hJPsi");
     	hTmpPsi2s = (TH1D*) filePsi2s->Get(Form("hMassPtDeltaPhi_%d-%d",i+1,j+1));
     	hTmpPsi2s -> SetName("hPsi2s");
     	fitJPsi = hTmpJPsi->GetFunction("fitJPsi");
     	fitPsi2s = hTmpPsi2s->GetFunction("fitPsi2s");
     	FitJPsi(hMassProjIncoh[i][j],kFALSE,2.4,5.,binPtValueRec[i],binDeltaPhiValueRec[j],-1,-1,-1,fitJPsi,fitPsi2s); 
     }
    }
  }
  // {
  // fileJPsi = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCJPsi_0_Pt%.2fDeltaPhi%.2f.root",minDeltaPhi,maxDeltaPhi));
  // filePsi2s = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCPsi2s_0_Pt%.2fDeltaPhi%.2f.root",minDeltaPhi,maxDeltaPhi));
  // TH1D *hTmpJPsi = 0, *hTmpPsi2s = 0;
  // hTmpJPsi = (TH1D*) fileJPsi->Get("hMassTot");
  // hTmpJPsi -> SetName("hJPsi");
  // hTmpPsi2s = (TH1D*) filePsi2s->Get("hMassTot");
  // hTmpPsi2s -> SetName("hPsi2s");
  // fitJPsi = hTmpJPsi->GetFunction("fitJPsi");
  // fitPsi2s = hTmpPsi2s->GetFunction("fitPsi2s");
  // FitJPsi(hMassTotCoh,kFALSE,2.45,4.,1,2,-1,-1,0,fitJPsi,fitPsi2s); 
  // hMassTotCoh->SaveAs("hMassTotDataCoh.C","RECREATE");
  // }

  // {
  // fileJPsi = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCJPsi_1_Pt%.2fDeltaPhi%.2f.root",minDeltaPhi,maxDeltaPhi));
  // filePsi2s = new TFile(Form("/gridgroup/ALICE/migliorin/MacroAnalysisJPsi/FitMC/FitMCPsi2s_1_Pt%.2fDeltaPhi%.2f.root",minDeltaPhi,maxDeltaPhi));
  // TH1D *hTmpJPsi = 0, *hTmpPsi2s = 0;
  // hTmpJPsi = (TH1D*) fileJPsi->Get("hMassTot");
  // hTmpJPsi -> SetName("hJPsi");
  // hTmpPsi2s = (TH1D*) filePsi2s->Get("hMassTot");
  // hTmpPsi2s -> SetName("hPsi2s");
  // fitJPsi = hTmpJPsi->GetFunction("fitJPsi");
  // fitJPsi -> SetLineColor(kBlack);	
  // fitPsi2s = hTmpPsi2s->GetFunction("fitPsi2s");
  // FitJPsi(hMassTotIncoh,kFALSE,2.45,4.,1,2,-1,-1,0,fitJPsi,fitPsi2s); 
  // hMassTotIncoh->SaveAs("hMassTotDataIncoh.C","RECREATE");
  // }     

  //  TH1D * h2 = new TH1D("h2","Fit Residuals",nBinMass,binMasseValueRec);
  //   TF1 *FitTotal=0;
  //  std::ostringstream os1;
  //  os1<<"TotalFit";
  //  fileFit->GetObject(os1.str().c_str(),FitTotal);
  // for(Int_t i=0;i<nBinMass;i++){
  //    double res =  (hMassTot->GetBinContent(i)- FitTotal->Eval(hMassTot->GetBinCenter(i)))/hMassTot->GetBinError(i);
  //   h2->SetBinContent(i,res);
  //   h2->SetBinError(i,1);
  // }
  // TCanvas * c1 = new TCanvas(); 
  // c1->Divide(1,2);
  // c1->cd(1);
  // hMassTot->GetFunction("TotalFit")->SetLineColor(kBlack);
  // hMassTot ->GetXaxis()->SetRangeUser(2.5,4);
  // hMassTot ->GetXaxis()->SetTitle("Inv Mass (GeV/c^{2})");	
  // hMassTot -> SetTitle("Inv Mass Data ( p_{T} < 0.25, #Delta#Phi -3.14 - 3.14)");
  // hMassTot->Draw();
  // // hMassProj[0][1]->Draw(); 
  // c1->cd(2);
  // h2->GetXaxis()->SetRangeUser(2.4,4);
  // h2->Draw("E");
}
