/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

// c++ headers
#include <iostream>
#include <fstream>
#include <map>

// root headers
#include <TMath.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TF2.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TTree.h>
#include <TGraph2D.h>
#include <TStopwatch.h>
#include <TMatrixDSym.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TClonesArray.h>
#include <TDatabasePDG.h>
#include <TLorentzVector.h>
#include <TParticle.h>
#include <TObjString.h>
#include <TList.h>
#include <TChain.h>
#include "THnSparse.h"

// aliroot headers
#include <AliAnalysisTask.h>
#include <AliAnalysisManager.h>
#include <AliAODEvent.h>
#include <AliMCEvent.h>
#include <AliMCParticle.h>
#include <AliAODInputHandler.h>
#include <AliMuonTrackCuts.h>

// my headers
#include "AliAnalysisTaskNanoJPsi2018.h"

class AliAnalysisTaskNanoJPsi2018;    // my analysis class

using namespace std;            // std namespace: so you can do things like 'cout'

ClassImp(AliAnalysisTaskNanoJPsi2018) // classimp: necessary for root


//_____________________________________________________________________________
AliAnalysisTaskNanoJPsi2018::AliAnalysisTaskNanoJPsi2018() : AliAnalysisTaskSE(),
  fMuonTrackCuts(0x0), fPeriod(0),
//fTriggerToAnalyse(0),
  fNTriggers(0),
  fIsMC(0), fAOD(0), fMC(0), fOutputList(0),fEvRecCounter(0), fNumberRecMuons(0), fNumberGenMuons(0), fEvGenCounter(0), fZNCEnergy(-999), fZNAEnergy(-999), fV0ADecision(-10), fV0CDecision(-10),fV0FiredCells(-10), fADADecision(-10), fADCDecision(-10), fPtMin(0), fGenPart(0),  fMuonTrackCounterH(0), fTracklets(0), fV0ATime(0), fV0CTime(0), fADATime(0), fADCTime(0),fNRecMuons(0), fPtMinMuon(0),fNGenMuons(0),fMuonMassPtGenRec(0),
 fMCRunNum(0), fMCMuMuPt(0),fMCMuMuPhi(0), fMCMuMuDeltaPhi(0),fMCMuMuY(0), fMCMuMuM(0),fMCMuPt1(0), fMCMuPt2(0),fMCCharge(0),
  fRunNum(0), fMuMuPt(0),fMuMuPhi(0), fMuMuDeltaPhi(0),fMuMuY(0), fMuMuM(0),fMuPt1(0), fMuPt2(0),fCharge(0),
fMCMuPhi1(0),fMCMuPhi2(0),fMCMuEta1(0),fMCMuEta2(0),
  fMuPhi1(0),fMuPhi2(0),fMuEta1(0),fMuEta2(0),fMCMuMuEta(0),fMuMuEta(0),
  fMCMuCharge1(0),fMCMuCharge2(0), fMuCharge1(0),fMuCharge2(0),fHistPtMuon(0)

{
    // default constructor, don't allocate memory here!
    // this is used by root for IO purposes, it needs to remain empty
  for(Int_t i=0 ; i<10; i++){
    fTriggerToAnalyse[i]="";
  }
for(Int_t i=0; i<20;i++){
    fIdxMuons[i]=-1;
    fIdxMCMuons[i]=-1;
  }

  for(Int_t i=0; i<2;i++){
    for(Int_t j=0; j<3;j++){
      fDiMuonKinematicsRec[i][j]=NULL;
      fDiMuonKinematicsGen[i][j]=NULL;
    }
  }
}
//_____________________________________________________________________________
AliAnalysisTaskNanoJPsi2018::AliAnalysisTaskNanoJPsi2018(const char* name) : AliAnalysisTaskSE(name),
									     fMuonTrackCuts(0x0), fPeriod(0), //fTriggerToAnalyse(0),
fNTriggers(0),
fIsMC(0), fAOD(0), fMC(0), fOutputList(0),fEvRecCounter(0), fNumberRecMuons(0),fNumberGenMuons(0), fPtMinMuon(0),fEvGenCounter(0), fZNCEnergy(-999), fZNAEnergy(-999), fV0ADecision(-10), fV0CDecision(-10), fV0FiredCells(-10), fADADecision(-10), fADCDecision(-10), fPtMin(0),    fGenPart(0),  fMuonTrackCounterH(0), fTracklets(0), fV0ATime(0), fV0CTime(0), fADATime(0), fADCTime(0),fNRecMuons(0),fNGenMuons(0),  fMuonMassPtGenRec(0),
  fMCRunNum(0), fMCMuMuPt(0),fMCMuMuPhi(0), fMCMuMuDeltaPhi(0),fMCMuMuY(0), fMCMuMuM(0),fMCMuPt1(0), fMCMuPt2(0),fMCCharge(0),
  fRunNum(0), fMuMuPt(0),fMuMuPhi(0), fMuMuDeltaPhi(0),fMuMuY(0), fMuMuM(0),fMuPt1(0), fMuPt2(0),fCharge(0),
  fMCMuPhi1(0),fMCMuPhi2(0),fMCMuEta1(0),fMCMuEta2(0),
  fMuPhi1(0),fMuPhi2(0),fMuEta1(0),fMuEta2(0),fMCMuMuEta(0),fMuMuEta(0),fMCMuCharge1(0),fMCMuCharge2(0), fMuCharge1(0),fMuCharge2(0),fHistPtMuon(0)



{
  // constructor 4 outputs
  DefineInput(0, TChain::Class());
  DefineOutput(1, TList::Class());
  for(Int_t i=0; i<10;i++){
    fTriggerToAnalyse[i]="";
  }
  for(Int_t i=0; i<20;i++){
    fIdxMuons[i]=-1;
    fIdxMCMuons[i]=-1;
  }
  for(Int_t i=0; i<2;i++){
    for(Int_t j=0; j<3;j++){
      fDiMuonKinematicsRec[i][j]=NULL;
      fDiMuonKinematicsGen[i][j]=NULL;
    }
  }
  DefineOutput(2, TTree::Class());
  DefineOutput(3, TTree::Class());
}
//_____________________________________________________________________________
AliAnalysisTaskNanoJPsi2018::~AliAnalysisTaskNanoJPsi2018()
{
  // destructor
  // liberate all allocated memory
  for(Int_t i=0; i<2;i++){
    for(Int_t j=0; j<3;j++){
      if(fDiMuonKinematicsRec[i][j]) delete fDiMuonKinematicsRec[i][j];
      if(fDiMuonKinematicsGen[i][j]) delete fDiMuonKinematicsGen[i][j];
    }
  }
  if(fOutputList) {delete fOutputList;}
  if(fMuonTrackCuts) {delete fMuonTrackCuts;}
  if(fEvRecCounter) {delete fEvRecCounter;}
  if(fEvGenCounter) {delete fEvGenCounter;}
  if(fNumberRecMuons) {delete fNumberRecMuons;}
  if(fNumberGenMuons) {delete fNumberGenMuons;}
  if(fMuonTrackCounterH) {delete fMuonTrackCounterH;}
  if(fMuonMassPtGenRec) {delete fMuonMassPtGenRec;}
  if(fTreeGen) {delete fTreeGen;}
  if(fTree) {delete fTree;}
  if(fHistPtMuon) {delete fHistPtMuon;}
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::UserCreateOutputObjects()
{
  // create output objects
  // this function is called ONCE at the start of your analysis (RUNTIME)

  ////////////////////////////////////////
  //Muon track cuts
  ////////////////////////////////////////
  fMuonTrackCuts = new AliMuonTrackCuts("StdMuonCuts", "StdMuonCuts");
  fMuonTrackCuts->SetFilterMask(AliMuonTrackCuts::kMuEta | AliMuonTrackCuts::kMuPdca | AliMuonTrackCuts::kMuMatchLpt);
  fMuonTrackCuts->SetAllowDefaultParams(kTRUE);
  fMuonTrackCuts->Print("mask");

  ////////////////////////////////////////
  //output histograms
  ////////////////////////////////////////
  fOutputList = new TList();          // this is a list which will contain all  histograms
  fOutputList->SetOwner(kTRUE);       // memory stuff: the list is owner of all objects it contains and will delete them
  //  counter for events passing each cut
  fEvRecCounter = new TH1F("fEvRecCounter", "fEvRecCounter", 25, 0., 25.);
  fOutputList->Add(fEvRecCounter);
  //counter for event MC gen
  fEvGenCounter = new TH1F("fEvGenCounter", "fEvGenCounter", 25, 0., 25.);
  fOutputList->Add(fEvGenCounter);
  //  counter for tracks passing each cut
  fMuonTrackCounterH = new TH1F("fMuonTrackCounterH", "fMuonTrackCounterH", 10, -0.5, 9.5);
  fOutputList->Add(fMuonTrackCounterH);
  // number of muons passing the muon selection
  fNumberRecMuons = new TH1D("fNumberRecMuons", "fNumberRecMuons", 12, 0., 12.);
  fOutputList->Add(fNumberRecMuons);
  fHistPtMuon = new TH1D("fHistPtMuonL","fHistPtMuonL",200,0.,20);
  fOutputList->Add(fHistPtMuon);
  if(fIsMC){
  fNumberGenMuons = new TH1D("fNumberGenMuons", "fNumberGenMuons", 12, 0., 12.);
    fOutputList->Add(fNumberGenMuons);
  fMuonMassPtGenRec= new TH2D("fMuonMassPtGenRec","fMuonMassPtGenRec",100,0.,2.,100,0.,2.);
  fOutputList->Add(fMuonMassPtGenRec);
  }

  //array of THnSparseD creation
  const Int_t nDim = 7;
  Int_t nBins[nDim] = {480,100,100,100,100,100,100};
  Double_t min[nDim] = {0.,0.,-4.5,-3.15,-3.15,0.,0.};
  Double_t max[nDim] = {12.,10.,-2.,3.15,3.15,10.,10.};
  const TString label[nDim] = {"MassDimuon","PtDimuon","Rapidity","Phi","Deltaphi","PtMuon1","PtMuon2"};

  fDiMuonKinematicsRec[0][0]  = new THnSparseD("fDiMuonKinematicsRec2MuOnlyOS",   "fDiMuonKinematicsRec2MuOnlyOS",   nDim, nBins, min, max);
  fDiMuonKinematicsRec[0][1]  = new THnSparseD("fDiMuonKinematicsRec2MuOnlyLSpp", "fDiMuonKinematicsRec2MuOnlyLSpp", nDim, nBins, min, max);
  fDiMuonKinematicsRec[0][2]  = new THnSparseD("fDiMuonKinematicsRec2MuOnlyLSmm", "fDiMuonKinematicsRec2MuOnlyLSmm", nDim, nBins, min, max);
  fDiMuonKinematicsRec[1][0]  = new THnSparseD("fDiMuonKinematicsRec3MoreMuOS",   "fDiMuonKinematicsRec3MoreMuOS",   nDim, nBins, min, max);
  fDiMuonKinematicsRec[1][1]  = new THnSparseD("fDiMuonKinematicsRec3MoreMuLSpp", "fDiMuonKinematicsRec3MoreMuLSpp", nDim, nBins, min, max);
  fDiMuonKinematicsRec[1][2]  = new THnSparseD("fDiMuonKinematicsRec3MoreMuLSmm", "fDiMuonKinematicsRec3MoreMuLSmm", nDim, nBins, min, max);

  if(fIsMC){
    fDiMuonKinematicsGen[0][0]  = new THnSparseD("fDiMuonKinematicsGen2MuOnlyOS",   "fDiMuonKinematicsGen2MuOnlyOS",   nDim, nBins, min, max);
    fDiMuonKinematicsGen[0][1]  = new THnSparseD("fDiMuonKinematicsGen2MuOnlyLSpp", "fDiMuonKinematicsGen2MuOnlyLSpp", nDim, nBins, min, max);
    fDiMuonKinematicsGen[0][2]  = new THnSparseD("fDiMuonKinematicsGen2MuOnlyLSmm", "fDiMuonKinematicsGen2MuOnlyLSmm", nDim, nBins, min, max);
    fDiMuonKinematicsGen[1][0]  = new THnSparseD("fDiMuonKinematicsGen3MoreMuOS",   "fDiMuonKinematicsGen3MoreMuOS",   nDim, nBins, min, max);
    fDiMuonKinematicsGen[1][1]  = new THnSparseD("fDiMuonKinematicsGen3MoreMuLSpp", "fDiMuonKinematicsGen3MoreMuLSpp", nDim, nBins, min, max);
    fDiMuonKinematicsGen[1][2]  = new THnSparseD("fDiMuonKinematicsGen3MoreMuLSmm", "fDiMuonKinematicsGen3MoreMuLSmm", nDim, nBins, min, max);
  }

  for (int i = 0; i < 2; ++i){
    for (int j = 0; j < 3; ++j){
      fDiMuonKinematicsRec[i][j]->Sumw2();
      if(fIsMC) fDiMuonKinematicsGen[i][j]->Sumw2();
      fOutputList->Add(fDiMuonKinematicsRec[i][j]);
      if(fIsMC) fOutputList->Add(fDiMuonKinematicsGen[i][j]);
      for(int k=0;k<nDim;k++){
	fDiMuonKinematicsRec[i][j]->GetAxis(k)->SetTitle(label[k]);
	if(fIsMC) fDiMuonKinematicsGen[i][j]->GetAxis(k)->SetTitle(label[k]);
      }

    }
  }
  // TTree creation
  fTree = new TTree("fTree", "fTree");
  fTreeGen = new TTree("fTreeGen", "fTreeGen");

  fTree ->Branch("fRunNum", &fRunNum, "fRunNum/I");
  fTree ->Branch("fMuMuPt", &fMuMuPt, "fMuMuPt/D");
  fTree ->Branch("fMuMuY", &fMuMuY, "fMuMuY/D");
  fTree ->Branch("fMuMuEta", &fMuMuEta, "fMuMuEta/D");
  fTree ->Branch("fMuMuM", &fMuMuM, "fMuMuM/D");
  fTree ->Branch("fMuMuPhi", &fMuMuPhi, "fMuMuPhi/D");
  fTree ->Branch("fMuMuDeltaPhi", &fMuMuDeltaPhi, "fMuMuDeltaPhi/D");
  fTree ->Branch("fCharge",&fCharge,"fCharge/I");
  fTree ->Branch("fMuPt1", &fMuPt1, "fMuPt1/D");
  fTree ->Branch("fMuPt2", &fMuPt2, "fMuPt2/D");
  fTree ->Branch("fMuPhi1", &fMuPhi1, "fMuPhi1/D");
  fTree ->Branch("fMuPhi2", &fMuPhi2, "fMuPhi2/D");
  fTree ->Branch("fMuEta1", &fMuEta1, "fMuEta1/D");
  fTree ->Branch("fMuEta2", &fMuEta2, "fMuEta2/D");
  fTree ->Branch("fMuCharge1", &fMuCharge1, "fMuCharge1/I");
  fTree ->Branch("fMuCharge2", &fMuCharge2, "fMuCharge2/I");
  
  if(fIsMC){
    fTree ->Branch("fMCRunNum", &fMCRunNum, "fMCRunNum/I");
    fTree ->Branch("fMCMuMuPt", &fMCMuMuPt, "fMCMuMuPt/D");
    fTree ->Branch("fMCMuMuY", &fMCMuMuY, "fMCMuMuY/D");
    fTree ->Branch("fMCMuMuEta", &fMCMuMuEta, "fMCMuMuEta/D");
    fTree ->Branch("fMCMuMuM", &fMCMuMuM, "fMCMuMuM/D");
    fTree ->Branch("fMCMuMuPhi", &fMCMuMuPhi, "fMCMuMuPhi/D");
    fTree ->Branch("fMCMuMuDeltaPhi", &fMCMuMuDeltaPhi, "fMCMuMuDeltaPhi/D");
    fTree ->Branch("fMCCharge",&fMCCharge,"fMCCharge/I");
    fTree ->Branch("fMCMuPt1", &fMCMuPt1, "fMCMuPt1/D");
    fTree ->Branch("fMCMuPt2", &fMCMuPt2, "fMCMuPt2/D");
    fTree ->Branch("fMCMuPhi1", &fMCMuPhi1, "fMCMuPhi1/D");
    fTree ->Branch("fMCMuPhi2", &fMCMuPhi2, "fMCMuPhi2/D");
    fTree ->Branch("fMCMuEta1", &fMCMuEta1, "fMCMuEta1/D");
    fTree ->Branch("fMCMuEta2", &fMCMuEta2, "fMCMuEta2/D");
    fTree ->Branch("fMCMuCharge1", &fMCMuCharge1, "fMCMuCharge1/I");
    fTree ->Branch("fMCMuCharge2", &fMCMuCharge2, "fMCMuCharge2/I");

    fTreeGen ->Branch("fMCRunNumGen", &fMCRunNum, "fMCRunNum/I");
    fTreeGen ->Branch("fMCMuMuPtGen", &fMCMuMuPt, "fMCMuMuPt/D");
    fTreeGen ->Branch("fMCMuMuYGen", &fMCMuMuY, "fMCMuMuY/D");
    fTreeGen ->Branch("fMCMuMuEtaGen", &fMCMuMuEta, "fMCMuMuEta/D");
    fTreeGen ->Branch("fMCMuMuMGen", &fMCMuMuM, "fMCMuMuM/D");
    fTreeGen ->Branch("fMCMuMuPhiGen", &fMCMuMuPhi, "fMCMuMuPhi/D");
    fTreeGen ->Branch("fMCMuMuDeltaPhiGen", &fMCMuMuDeltaPhi, "fMCMuMuDeltaPhi/D");
    fTreeGen ->Branch("fMCChargeGen",&fMCCharge,"fMCCharge/I");
    fTreeGen ->Branch("fMCMuPt1Gen", &fMCMuPt1, "fMCMuPt1/D");
    fTreeGen ->Branch("fMCMuPt2Gen", &fMCMuPt2, "fMCMuPt2/D");
    fTreeGen ->Branch("fMCMuPhi1Gen", &fMCMuPhi1, "fMCMuPhi1/D");
    fTreeGen ->Branch("fMCMuPhi2Gen", &fMCMuPhi2, "fMCMuPhi2/D");
    fTreeGen ->Branch("fMCMuEta1Gen", &fMCMuEta1, "fMCMuEta1/D");
    fTreeGen ->Branch("fMCMuEta2Gen", &fMCMuEta2, "fMCMuEta2/D");
    fTreeGen ->Branch("fMCMuCharge1Gen", &fMCMuCharge1, "fMCMuCharge1/I");
    fTreeGen ->Branch("fMCMuCharge2Gen", &fMCMuCharge2, "fMCMuCharge2Gen/I");

  }
  // post data
  PostData(1, fOutputList);
  PostData(2, fTree);
  PostData(3, fTreeGen);
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::NotifyRun(){
  /// Set run number for cuts
 fMuonTrackCuts->SetRun(fInputHandler);
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::PostAllData(){
  // Post data
   PostData(1, fOutputList);
   PostData(2, fTree);
   PostData(3, fTreeGen);
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::BuildGenDiMuons(){
  AliMCParticle *fMCPart[2]={0};
  printf("In method \n");
  Int_t charge=0;
  Double_t pt=0.;
  //fMCRunNum = fAOD->GetRunNumber();
  TLorentzVector MCmuons[2],MCdiMu,diffMCdiMu;
  for(int fGenMCPart=0;fGenMCPart<fNGenMuons;fGenMCPart++){
    fMCPart[0]= static_cast<AliMCParticle*>(fMC->GetTrack(fIdxMCMuons[fGenMCPart]));
    MCmuons[0].SetPxPyPzE(fMCPart[0]->Px(), fMCPart[0]->Py(), fMCPart[0]->Pz(), fMCPart[0]->E());
    for(int jGenMCPart=fGenMCPart+1;jGenMCPart<fNGenMuons;jGenMCPart++){
      fMCPart[1]= static_cast<AliMCParticle*>(fMC->GetTrack(fIdxMCMuons[jGenMCPart]));
      MCmuons[1].SetPxPyPzE(fMCPart[1]->Px(), fMCPart[1]->Py(), fMCPart[1]->Pz(), fMCPart[1]->E());
      MCdiMu=MCmuons[0]+MCmuons[1];
      if (fMCPart[0]->Charge()>0)  diffMCdiMu=MCmuons[0]-MCmuons[1];
      else  diffMCdiMu=MCmuons[1]-MCmuons[0];
      charge=fMCPart[0]->Charge()+fMCPart[1]->Charge();
      // fill in dimuon kinematics
      //      if (fNGenMuons==2){
      //	if (charge==0){
      fMCMuMuPt = MCdiMu.Pt();
      fMCMuMuPhi = MCdiMu.Phi();
      fMCMuMuY = MCdiMu.Rapidity();
      fMCMuMuEta=MCdiMu.Eta();
      fMCMuMuM = MCdiMu.M();
      fMCMuMuDeltaPhi=MCdiMu.DeltaPhi(diffMCdiMu);
      fMCMuPt1 =MCmuons[0].Pt();
      fMCMuPt2 =MCmuons[1].Pt();
      fMCCharge=charge;
      fMCMuPhi1=MCmuons[0].Phi();
      fMCMuPhi2=MCmuons[1].Phi();
      fMCMuEta1=MCmuons[0].Eta();
      fMCMuEta2=MCmuons[1].Eta();
      fMCMuCharge1=fMCPart[0]->Charge();
      fMCMuCharge2=fMCPart[1]->Charge();
    }
  }
}
//_____________________________________________________________________________

void AliAnalysisTaskNanoJPsi2018::BuildRecDiMuons(){
  // Get muon masss fromn PDG
  Double_t muonMass =TDatabasePDG::Instance()->GetParticle(13)->Mass();
  //fRunNum = fAOD->GetRunNumber();
  TLorentzVector muons[2],diMu,diffdiMu;
  Int_t charge=0;
  Double_t pt=0.;
  AliAODTrack *track[2] = {0};
  for (Int_t iTrack = 0; iTrack<fNRecMuons; iTrack++ ) {
    track[0]=static_cast<AliAODTrack*>(fAOD->GetTrack(fIdxMuons[iTrack]));
    muons[0].SetPtEtaPhiM(track[0]->Pt(), track[0]->Eta(), track[0]->Phi(), muonMass);
    for (Int_t jTrack = iTrack+1; jTrack< fNRecMuons; jTrack++ ) {
      track[1]=static_cast<AliAODTrack*>(fAOD->GetTrack(fIdxMuons[jTrack]));
      muons[1].SetPtEtaPhiM(track[1]->Pt(), track[1]->Eta(), track[1]->Phi(), muonMass);
      diMu=muons[0]+muons[1];
      if (track[0]->Charge()>0)  diffdiMu=muons[0]-muons[1];
      else  diffdiMu=muons[1]-muons[0];
      charge=track[0]->Charge()+track[1]->Charge();
      // fill in dimuon kinematics
      Double_t var[7] = {diMu.M(),diMu.Pt(),diMu.Rapidity(),diMu.Phi(),diMu.DeltaPhi(diffdiMu),muons[0].Pt(),muons[1].Pt()};
      //if (fNRecMuons==2){
      //if (charge==0){
	fMuMuPt = diMu.Pt();
	fMuMuPhi = diMu.Phi();
	fMuMuY = diMu.Rapidity();
	fMuMuEta=diMu.Eta();
	fMuMuM = diMu.M();
	fMuMuDeltaPhi=diMu.DeltaPhi(diffdiMu);
	fMuPt1 =muons[0].Pt();
	fMuPt2 =muons[1].Pt();
	fCharge=charge;
	fMuPhi1=muons[0].Phi();
	fMuPhi2=muons[1].Phi();
	fMuEta1=muons[0].Eta();
	fMuEta2=muons[1].Eta();
	fMuCharge1=track[0]->Charge();
	fMuCharge2=track[1]->Charge();
	//      }
      // }
    }
  }
}
// ----------------------------------------------------------------------------------------------------------------------------------
//

Bool_t AliAnalysisTaskNanoJPsi2018::CheckTrigger() {
  // check if the event is triggered according with the requested trigger contidition
  Bool_t result=kFALSE;
  // read trigger info
  TString trigger = fAOD->GetFiredTriggerClasses();
  for(Int_t itrig = 0; itrig < fNTriggers; itrig++){
    if (trigger.Contains(fTriggerToAnalyse[itrig].Data())) {
      result = kTRUE;
    }
  }

  return result;
}

Bool_t AliAnalysisTaskNanoJPsi2018::CheckV0AD() {
  // // read VO info

  AliVVZERO *dataVZERO = dynamic_cast<AliVVZERO*>(fAOD->GetVZEROData());
  if(!dataVZERO){
    fV0ADecision = 0;
    fV0CDecision = 0;
    return kFALSE;
  }else{
    fV0ADecision = dataVZERO->GetV0ADecision();
    fV0CDecision = dataVZERO->GetV0CDecision();
  }
  Int_t nV0FiredCells = 0;
  // get the number of fired V0C cells with GetBBFlag(i) method
  for(Int_t iV0Hits = 0; iV0Hits < 32; iV0Hits++) {
    if(dataVZERO->GetBBFlag(iV0Hits) == kTRUE) nV0FiredCells += 1;
  }
  fV0FiredCells = nV0FiredCells;

  //read AD data
  AliVAD *dataAD = dynamic_cast<AliVAD*>(fAOD->GetADData());
  if(!dataAD)  {
    fADADecision = 0;
    fADCDecision = 0;
    return kFALSE;
  }
  else {
    fADADecision = dataAD->GetADADecision();
    fADCDecision = dataAD->GetADCDecision();
  }

  Bool_t result=kFALSE;
  if (fV0ADecision == 0 && fV0FiredCells <= 2 && fADADecision == 0 && fADCDecision == 0  && (fV0CDecision == 0 || fV0CDecision == 1)) result = kTRUE;
  return result;
}
// ----------------------------------------------------------------------------------------------------------------------------------
Bool_t AliAnalysisTaskNanoJPsi2018::GoodMUONTrack(AliAODTrack* track){
  // selects good MUON tracks
  if (!track->IsMuonTrack()) return kFALSE;
  fMuonTrackCounterH->Fill(0);
  if(!fMuonTrackCuts->IsSelected(track))  return kFALSE;
  fMuonTrackCounterH->Fill(1);
  fHistPtMuon->Fill(track->Pt());
  if(!(track->Pt()>fPtMinMuon)) return kFALSE;
  fMuonTrackCounterH->Fill(2);
  return kTRUE;
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::UserExec(Option_t *)
{
  fNRecMuons=0,fNGenMuons=0;
  fMCMuMuPt=0,fMCMuMuPhi=0, fMCMuMuDeltaPhi=0,fMCMuMuY=0, fMCMuMuM=0,fMCMuPt1=0, fMCMuPt2=0,fMCCharge=0,fMuMuPt=0,fMuMuPhi=0, fMuMuDeltaPhi=0,fMuMuY=0, fMuMuM=0,fMuPt1=0, fMuPt2=0,fCharge=0,fMCMuPhi1=0,fMCMuPhi2=0,fMCMuEta1=0,fMCMuEta2=0,fMuPhi1=0,fMuPhi2=0,fMuEta1=0,fMuEta2=0,fMCMuMuEta=0,fMuMuEta=0,fMCMuCharge1=0,fMCMuCharge2=0, fMuCharge1=0,fMuCharge2=0, fRunNum=0, fMCRunNum=0;
  Int_t fEvTag=0;
  ////////////////////////////////////////////
  // general info of the event
  ////////////////////////////////////////////
  // get AOD event
  fAOD = 0;
  fAOD = dynamic_cast<AliAODEvent*>(InputEvent());
  if(!fAOD) {
    PostAllData();
    return;
  }
  fEvRecCounter->Fill(fEvTag++);  //AOD 1
  fRunNum = fMCRunNum = fAOD->GetRunNumber();

  Int_t listOfGoodRunNumbersLHC18q[] = { 295585, 295586, 295587, 295588, 295589, 295612, 295615, 295665, 295666, 295667,
					 295668, 295671, 295673, 295675, 295676, 295677, 295714, 295716, 295717, 295718,
					 295719, 295723, 295725, 295753, 295754, 295755, 295758, 295759, 295762, 295763,
					 295786, 295788, 295791, 295816, 295818, 295819, 295822, 295825, 295826, 295829,
					 295831, 295854, 295855, 295856, 295859, 295860, 295861, 295863, 295881, 295908,
					 295909, 295910, 295913, 295936, 295937, 295941, 295942, 295943, 295945, 295947,
					 296061, 296062, 296063, 296065, 296066, 296068, 296123, 296128, 296132, 296133,
					 296134, 296135, 296142, 296143, 296191, 296192, 296194, 296195, 296196, 296197,
					 296198, 296241, 296242, 296243, 296244, 296246, 296247, 296269, 296270, 296273,
					 296279, 296280, 296303, 296304, 296307, 296309, 296312, 296376, 296377, 296378,
					 296379, 296380, 296381, 296383, 296414, 296419, 296420, 296423, 296424, 296433,
					 296472, 296509, 296510, 296511, 296514, 296516, 296547, 296548, 296549, 296550,
					 296551, 296552, 296553, 296615, 296616, 296618, 296619, 296622, 296623 };
  Int_t listOfGoodRunNumbersLHC18r[] = { 296690, 296691, 296694, 296749, 296750, 296781, 296784, 296785, 296786, 296787,
					 296791, 296793, 296794, 296799, 296836, 296838, 296839, 296848, 296849, 296850,
					 296851, 296852, 296890, 296894, 296899, 296900, 296903, 296930, 296931, 296932,
					 296934, 296935, 296938, 296941, 296966, 296967, 296968, 296969, 296971, 296975,
					 296976, 296977, 296979, 297029, 297031, 297035, 297085, 297117, 297118, 297119,
					 297123, 297124, 297128, 297129, 297132, 297133, 297193, 297194, 297196, 297218,
					 297219, 297221, 297222, 297278, 297310, 297312, 297315, 297317, 297363, 297366,
					 297367, 297372, 297379, 297380, 297405, 297408, 297413, 297414, 297415, 297441,
					 297442, 297446, 297450, 297451, 297452, 297479, 297481, 297483, 297512, 297537,
					 297540, 297541, 297542, 297544, 297558, 297588, 297590, 297595, 297623, 297624 };
  Int_t listOfGoodRunNumbersLHC15o[] = { /*244918,*/ 244980, 244982, 244983, 245064, 245066, 245068, 245145, 245146, 245151,
                                         245152, 245231, 245232, 245233, 245253, 245259, 245343, 245345, 245346, 245347,
                                         245353, 245401, 245407, 245409, 245410, 245446, 245450, 245496, 245501, 245504,
                                         245505, 245507, 245535, 245540, 245542, 245543, 245554, 245683, 245692, 245700,
                                         245705, 245729, 245731, 245738, 245752, 245759, 245766, 245775, 245785, 245793,
                                         245829, 245831, 245833, 245949, 245952, 245954, 245963, 245996, 246001, 246003,
                                         246012, 246036, 246037, 246042, 246048, 246049, 246053, 246087, 246089, 246113,
                                         246115, 246148, 246151, 246152, 246153, 246178, 246181, 246182, 246217, 246220,
                                         246222, 246225, 246272, 246275, 246276, 246390, 246391, 246392, 246424, 246428,
                                         246431, 246433, 246434, 246487, 246488, 246493, 246495, 246675, 246676, 246750,
                                         246751, 246755, 246757, 246758, 246759, 246760, 246763, 246765, 246804, 246805,
                                         246806, 246807, 246808, 246809, 246844, 246845, 246846, 246847, 246851, 246855,
                                         246859, 246864, 246865, 246867, 246871, 246930, 246937, 246942, 246945, 246948,
                                         246949, 246980, 246982, 246984, 246989, 246991, 246994};
  Bool_t checkIfGoodRun = kFALSE;
  for( Int_t iRunLHC18q = 0; iRunLHC18q < 128; iRunLHC18q++){
    if( fRunNum == listOfGoodRunNumbersLHC18q[iRunLHC18q] ) checkIfGoodRun = kTRUE;
  }
  for( Int_t iRunLHC18r = 0; iRunLHC18r <  97; iRunLHC18r++){
    if( fRunNum == listOfGoodRunNumbersLHC18r[iRunLHC18r] ) checkIfGoodRun = kTRUE;
  }
  for( Int_t iRunLHC15o = 0; iRunLHC15o < 136/*137*/; iRunLHC15o++){
    if( fRunNum == listOfGoodRunNumbersLHC15o[iRunLHC15o] ) checkIfGoodRun = kTRUE;
  }
  if(checkIfGoodRun != 1) {
    PostAllData();
    return;
  }

  if(!fIsMC) {
    if(!CheckTrigger()){
      PostAllData();
      return;
    }
  }
  fEvRecCounter->Fill(fEvTag++); // right trigger
  if(!fIsMC) {
    if(!CheckV0AD()){
      PostAllData();
      return;
    }
  }
  fEvRecCounter->Fill(fEvTag++); // right trigger

  if(fIsMC){
  ////////////////////////////////////////////
  //  MC generated particles analysis
  ////////////////////////////////////////////
    fMC =0;
    fMC = dynamic_cast<AliMCEvent*>(MCEvent());
    printf("MC \n");
    if(!fMC){
      PostAllData();
      return;
    }
    fEvGenCounter->Fill(2); // MC generated event found

    Int_t nMCParticles(fMC->GetNumberOfTracks());
    if(nMCParticles<1) {
      PostAllData();
      return;
    }
    fEvGenCounter->Fill(3); // At least one MC generated particle

    // loop over MC tracks and select muons

    //Int_t *idxMCMuons = new Int_t[nMCParticles];
    for(Int_t iMCParticle = 0; iMCParticle < nMCParticles; iMCParticle++) {
      // get track
      AliMCParticle *MCPart = static_cast<AliMCParticle*>(fMC->GetTrack(iMCParticle));
      if(!MCPart) return; //plutot continue;
      // Particle is primary  for is not primary and it is coming from J/Psi or Psi' decay (for LHC18l7)
      if(MCPart->GetMother() == -1){
        // if muons increase counter and store indices
        if(MCPart->PdgCode() == 13 || MCPart->PdgCode() == -13){
          fIdxMCMuons[fNGenMuons] = iMCParticle;
          fNGenMuons++;
        }
      } else {
        AliMCParticle *MCMother = static_cast<AliMCParticle*>(fMC->GetTrack(MCPart->GetMother()));
        if(MCMother->PdgCode() != 443 && MCMother->PdgCode() != 100443) continue;
        // if muons increase counter and store indices
        if(MCPart->PdgCode() == 13 || MCPart->PdgCode() == -13){
          fIdxMCMuons[fNGenMuons] = iMCParticle;
          fNGenMuons++;
        }
      }
    }
    // store number of muons
    fNumberGenMuons->Fill(fNGenMuons);
    ////////////////////////////////////////////
    // two MC muon analysis
    ////////////////////////////////////////////
    if (fNGenMuons !=2) {
      PostAllData();
      return;
    }
    fEvGenCounter->Fill(4); //if we have at least mc generated
    printf("Method BuildGen \n");
    BuildGenDiMuons(); // analysis of gene dimuons
    fTreeGen->Fill();
  }

 ////////////////////////////////////////////
  //  Reconstructed tracks analysis
  ////////////////////////////////////////////
  if(!fAOD) {
    PostAllData();
    return;
  }
  fEvRecCounter->Fill(fEvTag++);  //AOD

  Int_t nTracks(fAOD->GetNumberOfTracks());
  if(nTracks<1) {
    PostAllData();
    return;
  }
  fEvRecCounter->Fill(fEvTag++); // At least one track
  if(!fIsMC){
    fTracklets = fAOD->GetTracklets()->GetNumberOfTracklets();

    if(fTracklets != 0){
      PostAllData();
      return;
    }
    fEvRecCounter->Fill(fEvTag++);
  }
  // loop over tracks and select good muons
  for(Int_t iTrack = 0; iTrack < nTracks; iTrack++) {
    // get track
    AliAODTrack* track = static_cast<AliAODTrack*>(fAOD->GetTrack(iTrack));
    if(!track) return; //plutot continue
    if((track->GetRAtAbsorberEnd() < 17.5) || (track->GetRAtAbsorberEnd() > 89.5)) continue;

    // increase counter and store indices
    if (GoodMUONTrack(track)) fIdxMuons[fNRecMuons++]=iTrack;
  }
  // store number of muons
  fNumberRecMuons->Fill(fNRecMuons);
  if(fNRecMuons!=2){
    PostAllData();
    return;
  }
  BuildRecDiMuons();
  fEvRecCounter->Fill(fEvTag++);
  fTree->Fill();
  // post the data
  PostAllData();
}
// ----------------------------------------------------------------------------------------------------------------------------------
void AliAnalysisTaskNanoJPsi2018::Terminate(Option_t *)
{
    cout << endl;
    // terminate
    // called at the END of the analysis (when all events are processed)
}
// ----------------------------------------------------------------------------------------------------------------------------------
