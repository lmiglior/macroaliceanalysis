// usage:
// .L plot.C
// plotFits(....) plot all invariant mass fits in data, vs cos(theta),phi,pT,y



void plotFitsMass(TList *l);
void plotFitsVar(TList *l, const char *var, const char *varText);
void plotMCClosureVar(TList *l, const char *var, const char *varText);

void plotFits(const char *fdatapath = "LHC18qr_Data", const char *fmcpath = "LHC18l7_JPsi_Incoh",
	  Double_t mumuPtmin = 0.35, Double_t mumuPtmax = 1.35, Double_t muPtmin = 0.0,
	  Int_t extractOpt = 0,
	  Int_t iteration = 0,
	  Int_t n1 = 0, Int_t n2 = -1,
		Bool_t reweightTrig = kFALSE)
{
  TFile *f = TFile::Open("results.root");
  TList *l = (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_%d_%d_%d_%d_%d",
				  fdatapath,fmcpath,
				  mumuPtmin,mumuPtmax,muPtmin,
				  extractOpt,iteration,n1,n2,reweightTrig));
  plotFitsMass(l);
  plotFitsVar(l,"Theta","cos#theta");
  plotFitsVar(l,"Phi","#varphi");
  plotFitsVar(l,"Pt","p_{T}");
  plotFitsVar(l,"Y","y");
}

void plotFitsMass(TList *l)
{
  TCanvas *c1 = new TCanvas("cMass","");
  c1->Divide(2,1);
  {
    c1->cd(1);
    TH1D *h1 = (TH1D *)l->FindObject("hMMC");
    h1->SetStats(kFALSE);
    h1->Draw();
  }
  {
    c1->cd(2);
    TH1D *h1 = (TH1D *)l->FindObject("hMData");
    TH1D *h2 = (TH1D *)l->FindObject("hMData_res");
    h1->SetStats(kFALSE);
    h1->Draw();
    std::cout<<h1->GetBinWidth(1)<<std::endl;
    TLatex *l1 = new TLatex;
    l1->DrawLatexNDC(0.5,0.7,Form("N_{J/#psi} = %.0f #pm %.0f",h2->GetBinContent(3),h2->GetBinError(3)));
    l1->DrawLatexNDC(0.5,0.6,Form("N_{bkg}^{#pm3#sigma} = %.0f #pm %.0f",h2->GetBinContent(6),h2->GetBinError(6)));
    l1->DrawLatexNDC(0.5,0.5,Form("#chi^{2}/ndf = %.1f / %.0f",h2->GetBinContent(4),h2->GetBinContent(5)));
  }
}

void plotFitsVar(TList *l, const char *var, const char *varText)
{
  // example of plotting inv mass fit in bins of cos(theta)
  TCanvas *c2 = new TCanvas(Form("c%s",var),"",1200,1000);
  c2->Divide(4,4);
  Int_t j = 1;
  for(Int_t i = 1; i <= 25; ++i) {
    TH1D *h1 = (TH1D *)l->FindObject(Form("h%sData_M_%d",var,i));
    TH1D *h2 = (TH1D *)l->FindObject(Form("h%sData_M_%d_res",var,i));
    if (!h2 || h2->GetBinContent(5)==0) continue; // no fit is performed, because of low or 0 stat or index out of range
    c2->cd(j);
    h1->SetStats(0);
    h1->Draw();
    TLatex *l1 = new TLatex;
    l1->DrawLatexNDC(0.4,0.8,Form("%.2f < %s < %.2f",h2->GetBinContent(1),varText,h2->GetBinContent(2)));
    l1->DrawLatexNDC(0.5,0.7,Form("N_{J/#psi} = %.0f #pm %.0f",h2->GetBinContent(3),h2->GetBinError(3)));
    l1->DrawLatexNDC(0.5,0.6,Form("N_{bkg}^{#pm3#sigma} = %.0f #pm %.0f",h2->GetBinContent(6),h2->GetBinError(6)));
    l1->DrawLatexNDC(0.5,0.5,Form("#chi^{2}/ndf = %.1f / %.0f",h2->GetBinContent(4),h2->GetBinContent(5)));
    j++;
  }
}

void plotMCClosure(const char *fdatapath = "LHC18qr_Data", const char *fmcpath = "LHC18l7_JPsi_Incoh",
		   Double_t mumuPtmin = 0.35, Double_t mumuPtmax = 1.35, Double_t muPtmin = 0.0,
		   Int_t extractOpt = 0,
		   Int_t iteration = 0,
		   Int_t n1 = 1, Int_t n2 = 2)
{
  TFile *f = TFile::Open("results.root");
  TList *l = (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_%d_%d_%d_%d",
				  fdatapath,fmcpath,
				  mumuPtmin,mumuPtmax,muPtmin,
				  extractOpt,iteration,n1,n2));

  plotMCClosureVar(l,"Theta","cos#theta");
  plotMCClosureVar(l,"Phi","#varphi");
}

void plotMCClosureVar(TList *l, const char *var, const char *varText)
{
  TCanvas *c1 = new TCanvas(Form("c%sClosure",var),"");
  c1->Divide(1,2);
  TH1D *hGen = (TH1D *)l->FindObject(Form("h%sGen1",var));
  TH1D *hCorr = (TH1D *)l->FindObject(Form("h%sMC_Jpsi_corr",var));
  TH1D *hCorrWoUnf = (TH1D *)l->FindObject(Form("h%sMC_Jpsi_corrWoUnf",var));
  hGen->SetLineColor(kBlue);
  hCorr->SetLineColor(kRed);
  hCorrWoUnf->SetLineColor(kGreen+2);
  c1->cd(1);
  hGen->GetXaxis()->SetTitle(varText);
  hGen->Draw();
  hCorr->Draw("same");
  hCorrWoUnf->Draw("same");
  c1->cd(2);
  TH1D *hCorrRatio = (TH1D *)hCorr->Clone("hCorrRatio");
  hCorrRatio->Divide(hGen);
  hCorrRatio->GetYaxis()->SetRangeUser(0.8,1.2);
  hCorrRatio->Draw();
  TH1D *hCorrWoUnfRatio = (TH1D *)hCorrWoUnf->Clone("hCorrWoUnfRatio");
  hCorrWoUnfRatio->Divide(hGen);
  hCorrWoUnfRatio->Draw("same");

  TCanvas *c2 = new TCanvas(Form("c%sUnc",var),"");
  TH1D *hUnc = (TH1D *)hCorr->Clone("hUnc");
  hUnc->Reset();
  // calculate ratio of uncertainties of unfolding vs w/o unfolding results
  for(Int_t i = 1; i <= hUnc->GetNbinsX(); ++i)
    if (hCorr->GetBinError(i)>0 && hCorrWoUnf->GetBinError(i)) hUnc->SetBinContent(i,hCorr->GetBinError(i)/hCorrWoUnf->GetBinError(i));
  hUnc->Draw();
}
