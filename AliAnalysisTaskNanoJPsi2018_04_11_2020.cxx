/**************************************************************************
 * Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/

// c++ headers
#include <iostream>
#include <fstream>
#include <map>

// root headers
#include <TMath.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TF2.h>
#include <TF1.h>
#include <TRandom.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TTree.h>
#include <TGraph2D.h>
#include <TStopwatch.h>
#include <TMatrixDSym.h>
#include <TFitResult.h>
#include <TLatex.h>
#include <TClonesArray.h>
#include <TDatabasePDG.h>
#include <TLorentzVector.h>
#include <TParticle.h>
#include <TObjString.h>
#include <TList.h>
#include <TChain.h>

// aliroot headers
#include <AliAnalysisTask.h>
#include <AliAnalysisManager.h>
#include <AliAODEvent.h>
#include <AliMCEvent.h>
#include <AliMCParticle.h>
#include <AliAODInputHandler.h>
#include <AliMuonTrackCuts.h>

// my headers
#include "AliAnalysisTaskNanoJPsi2018.h"

class AliAnalysisTaskNanoJPsi2018;    // my analysis class

using namespace std;            // std namespace: so you can do things like 'cout'

ClassImp(AliAnalysisTaskNanoJPsi2018) // classimp: necessary for root

//_____________________________________________________________________________
AliAnalysisTaskNanoJPsi2018::AliAnalysisTaskNanoJPsi2018() : AliAnalysisTaskSE(),
 fMuonTrackCuts(0x0), fPeriod(0), fTrigger(0), fIsMC(0), fIsScalingOn(0), fAOD(0), fMC(0), fOutputList(0),fCounterH(0), fNumberMuonsH(0), fNumberMCMuonsH(0),
 fMapAnalysedMC(), fRecTree(0), fRunNum(0), fL0inputs(0),  fZNCEnergy(-999), fZNAEnergy(-999),   fV0ADecision(-10), fV0CDecision(-10),  fV0AFiredCells(-10),
 fV0CFiredCells(-10), fADADecision(-10), fADCDecision(-10), fIsZNAFired(-10), fIsZNCFired(-10),  fMuMuPt(0), fMuMuY(0), fMuMuM(0),   fCMUP6Decision(-10),
 fCMUP26Decision(-10), fCMUP11Decision(-10), fGenPart(0), fGenTree(0), fMCRunNum(0), fMCMuMuPt(0),   fMCMuMuY(0), fMCMuMuM(0), fTrgTree(0), fTrgRunNum(0),
 fCMUP6(-10), fCMUP26(-10), fCMUP11(-10), fMuonTrackCounterH(0),fGoodTrkFwdH(0),fTriggerCounterFwdH(0),fMuMuPhi(0),
 fMuPt1(0), fMuPt2(0), fMuEta1(0), fMuEta2(0), fMuPhi1(0), fMuPhi2(0), fMuQ1(0), fMuQ2(0),fRAbsMuonH(0), fMuMuMassPtH(0),fMuMuMass(0),
 fMCMuPt1(0), fMCMuPt2(0), fMCMuEta1(0), fMCMuEta2(0), fMCMuPhi1(0), fMCMuPhi2(0), fMCMuPDG1(0), fMCMuPDG2(0),fMCMuMuPhi(0),fTracklets(0),
 fV0ATime(0), fV0CTime(0), fADATime(0), fADCTime(0)


{
    // default constructor, don't allocate memory here!
    // this is used by root for IO purposes, it needs to remain empty
}
//_____________________________________________________________________________
AliAnalysisTaskNanoJPsi2018::AliAnalysisTaskNanoJPsi2018(const char* name) : AliAnalysisTaskSE(name),
  fMuonTrackCuts(0x0), fPeriod(0), fTrigger(0), fIsMC(0), fIsScalingOn(0), fAOD(0), fMC(0), fOutputList(0),fCounterH(0), fNumberMuonsH(0), fNumberMCMuonsH(0),
  fMapAnalysedMC(), fRecTree(0), fRunNum(0), fL0inputs(0), fZNCEnergy(-999), fZNAEnergy(-999), fV0ADecision(-10), fV0CDecision(-10),  fV0AFiredCells(-10),
  fV0CFiredCells(-10), fADADecision(-10), fADCDecision(-10), fIsZNAFired(-10), fIsZNCFired(-10), fMuMuPt(0), fMuMuY(0), fMuMuM(0), fCMUP6Decision(-10),
  fCMUP26Decision(-10), fCMUP11Decision(-10), fGenPart(0), fGenTree(0), fMCRunNum(0), fMCMuMuPt(0), fMCMuMuY(0), fMCMuMuM(0), fTrgTree(0), fTrgRunNum(0),
  fMuPt1(0), fMuPt2(0), fMuEta1(0), fMuEta2(0), fMuPhi1(0), fMuPhi2(0), fMuQ1(0), fMuQ2(0),fRAbsMuonH(0), fMuMuMassPtH(0),fTracklets(0),
  fCMUP6(-10), fCMUP26(-10), fCMUP11(-10), fMuonTrackCounterH(0),fGoodTrkFwdH(0), fTriggerCounterFwdH(0),fMuMuPhi(0),fMuMuMass(0),
  fMCMuPt1(0), fMCMuPt2(0), fMCMuEta1(0), fMCMuEta2(0), fMCMuPhi1(0), fMCMuPhi2(0), fMCMuPDG1(0), fMCMuPDG2(0),fMCMuMuPhi(0),
  fV0ATime(0), fV0CTime(0), fADATime(0), fADCTime(0)



{
  // constructor 4 outputs
  DefineInput(0, TChain::Class());
  DefineOutput(1, TTree::Class());
  DefineOutput(2, TList::Class());
  DefineOutput(3, TTree::Class());
  DefineOutput(4, TTree::Class());
}
//_____________________________________________________________________________
AliAnalysisTaskNanoJPsi2018::~AliAnalysisTaskNanoJPsi2018()
{
  // destructor
  // liberate all allocated memory
  if(fOutputList) {delete fOutputList;}
  if(fMuonTrackCuts) {delete fMuonTrackCuts;}
  if(fRecTree) {delete fRecTree;}
  if(fGenTree) {delete fGenTree;}
  if(fTrgTree) {delete fTrgTree;}
  if(fCounterH) {delete fCounterH;}
  if(fNumberMuonsH) {delete fNumberMuonsH;}
  if(fNumberMCMuonsH) {delete fNumberMCMuonsH;}
  if(fMuonTrackCounterH) {delete fMuonTrackCounterH;}
  if(fTriggerCounterFwdH) {delete fTriggerCounterFwdH;}
  if(fGoodTrkFwdH) {delete fGoodTrkFwdH;}
  if(fRAbsMuonH) {delete fRAbsMuonH;}
  if(fMuMuMassPtH) {delete fMuMuMassPtH;}
  if(fMuMuMass){delete fMuMuMass;}
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::UserCreateOutputObjects()
{
  // create output objects
  // this function is called ONCE at the start of your analysis (RUNTIME)

  ////////////////////////////////////////
  //Muon track cuts
  ////////////////////////////////////////
  fMuonTrackCuts = new AliMuonTrackCuts("StdMuonCuts", "StdMuonCuts");
  fMuonTrackCuts->SetFilterMask(AliMuonTrackCuts::kMuEta | AliMuonTrackCuts::kMuPdca | AliMuonTrackCuts::kMuMatchLpt);
  fMuonTrackCuts->SetAllowDefaultParams(kTRUE);
  fMuonTrackCuts->Print("mask");

  ////////////////////////////////////////
  //Analysed output tree
  ////////////////////////////////////////
  fRecTree = new TTree("fRecTree", "fRecTree");
  fRecTree ->Branch("fRunNum", &fRunNum, "fRunNum/I");
  fRecTree ->Branch("fL0inputs", &fL0inputs, "fL0inputs/i");
  fRecTree ->Branch("fZNCEnergy", &fZNCEnergy, "fZNCEnergy/F");
  fRecTree ->Branch("fZNAEnergy", &fZNAEnergy, "fZNAEnergy/F");
  fRecTree ->Branch("fZNATDC", &fZNATDC[0], "fZNATDC[4]/F");
  fRecTree ->Branch("fZNCTDC", &fZNCTDC[0], "fZNCTDC[4]/F");
  fRecTree ->Branch("fTracklets", &fTracklets, "fTracklets/I");
  fRecTree ->Branch("fV0ADecision", &fV0ADecision, "fV0ADecision/I");
  fRecTree ->Branch("fV0CDecision", &fV0CDecision, "fV0CDecision/I");
  fRecTree ->Branch("fV0AFiredCells", &fV0AFiredCells, "fV0AFiredCells/I");
  fRecTree ->Branch("fV0CFiredCells", &fV0CFiredCells, "fV0CFiredCells/I");
  fRecTree ->Branch("fADADecision", &fADADecision, "fADADecision/I");
  fRecTree ->Branch("fADCDecision", &fADCDecision, "fADCDecision/I");
  fRecTree ->Branch("fIsZNAFired", &fIsZNAFired, "fIsZNAFired/I");
  fRecTree ->Branch("fIsZNCFired", &fIsZNCFired, "fIsZNCFired/I");
  fRecTree ->Branch("fMuMuPt", &fMuMuPt, "fMuMuPt/F");
  fRecTree ->Branch("fMuMuPhi", &fMuMuPhi, "fMuMuPhi/D");
  fRecTree ->Branch("fMuMuY", &fMuMuY, "fMuMuY/F");
  fRecTree ->Branch("fMuMuM", &fMuMuM, "fMuMuM/F");
  fRecTree ->Branch("fCMUP6Decision", &fCMUP6Decision, "fCMUP6Decision/I");
  fRecTree ->Branch("fCMUP26Decision", &fCMUP26Decision, "fCMUP26Decision/I");
  fRecTree ->Branch("fCMUP11Decision", &fCMUP11Decision, "fCMUP11Decision/I");
  fRecTree ->Branch("fV0ADecision", &fV0ADecision, "fV0ADecision/I");
  fRecTree ->Branch("fV0CDecision", &fV0CDecision, "fV0CDecision/I");
  fRecTree ->Branch("fV0ATime", &fV0ATime, "fV0ATime/D");
  fRecTree ->Branch("fV0CTime", &fV0CTime, "fV0CTime/D");
  fRecTree ->Branch("fV0AMultiplicity", &fV0AMultiplicity[0], "fV0AMultiplicity[32]/F");
  fRecTree ->Branch("fV0CMultiplicity", &fV0CMultiplicity[0], "fV0CMultiplicity[32]/F");
  fRecTree ->Branch("fADADecision", &fADADecision, "fADADecision/I");
  fRecTree ->Branch("fADCDecision", &fADCDecision, "fADCDecision/I");
  fRecTree ->Branch("fADATime", &fADATime, "fADATime/D");
  fRecTree ->Branch("fADCTime", &fADCTime, "fADCTime/D");
  fRecTree ->Branch("fADAMultiplicity", &fADAMultiplicity[0], "fADAMultiplicity[8]/F");
  fRecTree ->Branch("fADCMultiplicity", &fADCMultiplicity[0], "fADCMultiplicity[8]/F");
  fRecTree ->Branch("fMuPt1", &fMuPt1, "fMuPt1/D");
  fRecTree ->Branch("fMuPt2", &fMuPt2, "fMuPt2/D");
  fRecTree ->Branch("fMuEta1", &fMuEta1, "fMuEta1/D");
  fRecTree ->Branch("fMuEta2", &fMuEta2, "fMuEta2/D");
  fRecTree ->Branch("fMuPhi1", &fMuPhi1, "fMuPhi1/D");
  fRecTree ->Branch("fMuPhi2", &fMuPhi2, "fMuPhi2/D");
  fRecTree ->Branch("fMuQ1", &fMuQ1, "fMuQ1/D");
  fRecTree ->Branch("fMuQ2", &fMuQ2, "fMuQ2/D");
  // post data
  PostData(1, fRecTree);

  ////////////////////////////////////////
  //MC generated output tree
  ////////////////////////////////////////
  fGenTree = new TTree("fGenTree", "fGenTree");
  if(fIsMC){
    fGenPart = new TClonesArray("TParticle", 1000);
    fGenTree ->Branch("fMCRunNum", &fMCRunNum, "fMCRunNum/I");
    fGenTree ->Branch("fMCMuMuPt", &fMCMuMuPt, "fMCMuMuPt/F");
    fGenTree ->Branch("fMCMuMuPhi", &fMCMuMuPhi, "fMCMuMuPhi/D");
    fGenTree ->Branch("fMCMuMuY", &fMCMuMuY, "fMCMuMuY/F");
    fGenTree ->Branch("fMCMuMuM", &fMCMuMuM, "fMCMuMuM/F");
    fGenTree ->Branch("fMCMuMuPhi", &fMCMuMuPhi, "fMCMuMuPhi/D");
    fGenTree ->Branch("fMCMuMuY", &fMCMuMuY, "fMCMuMuY/F");
    fGenTree ->Branch("fMCMuMuM", &fMCMuMuM, "fMCMuMuM/F");
    fGenTree ->Branch("fMCMuPt1", &fMCMuPt1, "fMCMuPt1/D");
    fGenTree ->Branch("fMCMuPt2", &fMCMuPt2, "fMCMuPt2/D");
    fGenTree ->Branch("fMCMuEta1", &fMCMuEta1, "fMCMuEta1/D");
    fGenTree ->Branch("fMCMuEta2", &fMCMuEta2, "fMCMuEta2/D");
    fGenTree ->Branch("fMCMuPhi1", &fMCMuPhi1, "fMCMuPhi1/D");
    fGenTree ->Branch("fMCMuPhi2", &fMCMuPhi2, "fMCMuPhi2/D");
    fGenTree ->Branch("fMCMuPDG1", &fMCMuPDG1, "fMCMuPDG1/D");
    fGenTree ->Branch("fMCMuPDG2", &fMCMuPDG2, "fMCMuPDG2/D");
    // post data
  }
  PostData(3, fGenTree);

  ////////////////////////////////////////
  //Trigger information tree
  ////////////////////////////////////////
  fTrgTree = new TTree("fTrgTree", "fTrgTree");
  if(!fIsMC){
    fTrgTree ->Branch("fTrgRunNum", &fTrgRunNum, "fTrgRunNum/I");
    fTrgTree ->Branch("fCMUP6", &fCMUP6, "fCMUP6/I");
    fTrgTree ->Branch("fCMUP26", &fCMUP26, "fCMUP26/I");
    fTrgTree ->Branch("fCMUP11", &fCMUP11, "fCMUP11/I");
    // post data
  }
  PostData(4, fTrgTree);

  ////////////////////////////////////////
  //output histograms
  ////////////////////////////////////////
  fOutputList = new TList();          // this is a list which will contain all  histograms
  fOutputList->SetOwner(kTRUE);       // memory stuff: the list is owner of all objects it contains and will delete them
  //  counter for events passing each cut
  fCounterH = new TH1F("fCounterH", "fCounterH", 25, 0., 25.);
  fOutputList->Add(fCounterH);
  //  counter for tracks passing each cut
  fMuonTrackCounterH = new TH1F("fMuonTrackCounterH", "fMuonTrackCounterH", 10, -0.5, 9.5);
  fOutputList->Add(fMuonTrackCounterH);
  // number of positive and negative muons passing the muon selection
  fNumberMuonsH = new TH2F("fNumberMuonsH", "fNumberMuonsH", 12, 0., 12.,12, 0., 12.);
  fOutputList->Add(fNumberMuonsH);
  // number of positive and negative MC muons passing the muon selection
  if(fIsMC){
    fNumberMCMuonsH = new TH2F("fNumberMCMuonsH", "fNumberMCMuonsH", 12, 0., 12.,12, 0., 12.);
    fOutputList->Add(fNumberMCMuonsH);
  }
  // Rabs of positive and negative muons passing the muon selection
  fRAbsMuonH = new TH2F("fRAbsMuonH", "fRAbsMuonH", 100, 0, 100, 100, 0, 100);
  fOutputList->Add(fRAbsMuonH);
  // kinematics of dimouns
  fMuMuMassPtH = new TH2F("fMuMuMassPtH", "fMuMuMassPtH", 1500, 0, 150, 150, 0, 15);
  fMuMuMass = new TH1F("fMuMuMass","fMuMuMass",480,0.,12.);
  fOutputList->Add(fMuMuMassPtH);
  fOutputList->Add(fMuMuMass);
  //  counter for triggers per run
  Int_t FirstRun = 295530;
  Int_t LastRun = 297635;
  Int_t nRuns = LastRun-FirstRun+1;
  fTriggerCounterFwdH = new TH1F("fTriggerCounterFwdH", "fTriggerCounterFwdH", nRuns, FirstRun-0.5, LastRun+0.5);
  fOutputList->Add(fTriggerCounterFwdH);
  // number of good tracks in event
  fGoodTrkFwdH = new TH2F("fGoodTrkFwdH", "fGoodTrkFwdH", 10,-0.5,9.5,10,-0.5,9.5);
  fOutputList->Add(fGoodTrkFwdH);

  // post data
  PostData(2, fOutputList);
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::NotifyRun()
{
  /// Set run number for cuts
 fMuonTrackCuts->SetRun(fInputHandler);
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::PostAllData()
{
  // Post data
  PostData(1, fRecTree);
  PostData(2, fOutputList);
  PostData(3, fGenTree);
  PostData(4, fTrgTree);
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::SetPeriod(Int_t period)
{
  // period = 0 => 2018 q, = 1 => 2018 r
  fPeriod = period;
}

//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::SetMC(Bool_t flag)
{
  // set if MC file
  fIsMC = flag;
}
// ----------------------------------------------------------------------------------------------------------------------------------
void AliAnalysisTaskNanoJPsi2018::TwoMuonAna(Int_t *idxPosMuons, Int_t *idxNegMuons)
{
  // Get muon masss fromn PDG
  TDatabasePDG *pdgdat = TDatabasePDG::Instance();
  TParticlePDG *partMuon = pdgdat->GetParticle(13);
  Double_t MuonMass = partMuon->Mass();
  // create all four vectors
  // --  positive muon
  TLorentzVector PosMuon1;
  AliAODTrack *PosTrack = static_cast<AliAODTrack*>(fAOD->GetTrack(idxPosMuons[0]));
  PosMuon1.SetPtEtaPhiM(PosTrack->Pt(), PosTrack->Eta(), PosTrack->Phi(), MuonMass);
  // --  negative muon
  TLorentzVector NegMuon1;
  AliAODTrack *NegTrack = static_cast<AliAODTrack*>(fAOD->GetTrack(idxNegMuons[0]));
  NegMuon1.SetPtEtaPhiM(NegTrack->Pt(), NegTrack->Eta(), NegTrack->Phi(), MuonMass);
  // fill Rabs histo
  fRAbsMuonH->Fill(PosTrack->GetRAtAbsorberEnd(),NegTrack->GetRAtAbsorberEnd());
  // fill in dimuon kinematics
  TLorentzVector MuMu = NegMuon1+PosMuon1;
  fMuMuMassPtH->Fill(MuMu.M(),MuMu.Pt());
  fMuMuMass->Fill(MuMu.M());
  // set tree variables
  fMuMuPt = MuMu.Pt();
  fMuMuPhi = MuMu.Phi();
  fMuMuY = MuMu.Rapidity();
  fMuMuM = MuMu.M();
  fMuPt1 = PosTrack->Pt();
  fMuEta1 = PosTrack->Eta();
  fMuPhi1 = PosTrack->Phi();
  fMuQ1 = PosTrack->Charge();
  fMuPhi2 = NegTrack->Phi();
  fMuEta2 = NegTrack->Eta();
  fMuPt2 = NegTrack->Pt();
  fMuQ2 = NegTrack->Charge();
}
// ----------------------------------------------------------------------------------------------------------------------------------
//
// ----------------------------------------------------------------------------------------------------------------------------------

// Bool_t AliAnalysisTaskNanoJPsi2018::CheckTrigger()
// // checks if event is triggered according to period and analysis type
// {
void AliAnalysisTaskNanoJPsi2018::CheckTrigger(Bool_t *isTriggered)
// checks if event is triggered according to period and analysis type
{
  // Initialise: 0 = fwd, 1 = cent, 2 = semi-fwd
  isTriggered[0] = isTriggered[1] = isTriggered[2] = kFALSE;

  // read trigger info
  TString trigger = fAOD->GetFiredTriggerClasses();

  // forward analysis
  // in 2016 r : CMUP14-B-NOPF-MUFAST = 0MSL *0VBA *0UBA
  // in 2016 s : CMUP23-B-NOPF-MUFAST = 0MUL *0UBC *0UGC *0VBA *0VGA *0SH2 *0VC5
  // central analysis
  // in 2016 r CCUP20-B-NOPF-CENTNOTRD = !VBA & !VGA & !VBC & !UBA & !UGC & !SH2 & STG & OMU
  // in 2016 s CCUP22-B-SPD2-CENTNOTRD = !VBA & !VGA & !VBC & !UBC & !UGC & !SH2 & STG & OMU
  // semi-forward analysis
  // in 2016 r CMUP15-B-NOPF-ALLNOTRD = *0VBA *0UBA *0VC5 0SMB *0SH2 0MSL
  // in 2016 s CMUP22-B-NOPF-ALLNOTRD = *0UBC *0UGC *0VBA *0VGA *0SH2 *0VC5 0MSL 0SMB

  // MUON 2018
  // CMUP11-B-NOPF-MUFAST = *0VBA *0UBA *0UBC 0MUL
  // CMUP26-B-NOPF-MUFAST = *0VBA *0UBA *0UBC 0MLL
  // CMUP6-B-NOPF-MUFAST = *0VBA 0MUL

  // if (trigger.Contains("CMUP11-B-NOPF-MUFAST")) return kTRUE;
  // if (trigger.Contains("CMUP26-B-NOPF-MUFAST")) return kTRUE;
  // if (trigger.Contains("CMUP6-B-NOPF-MUFAST")) return kTRUE;
  if (trigger.Contains("CMUP11-B-NOPF-MUFAST"))isTriggered[0]  = kTRUE;
  if (trigger.Contains("CMUP26-B-NOPF-MUFAST")) isTriggered[1] = kTRUE;
  if (trigger.Contains("CMUP6-B-NOPF-MUFAST")) isTriggered[2] = kTRUE;

}
// ----------------------------------------------------------------------------------------------------------------------------------
Bool_t AliAnalysisTaskNanoJPsi2018::GoodMUONTrack(Int_t iTrack)
// selects good MUON tracks
{
  fMuonTrackCounterH->Fill(0);
  AliAODTrack* track = static_cast<AliAODTrack*>(fAOD->GetTrack(iTrack));
  if(!track) return kFALSE;
  fMuonTrackCounterH->Fill(1);
  if(!track->IsMuonTrack())  return kFALSE;
  fMuonTrackCounterH->Fill(2);
  if(!fMuonTrackCuts->IsSelected(track))  return kFALSE;
  fMuonTrackCounterH->Fill(3);
  if(track->GetRAtAbsorberEnd()>89.5)  return kFALSE;
  fMuonTrackCounterH->Fill(4);
  if(track->GetRAtAbsorberEnd()<17.5)  return kFALSE;
  fMuonTrackCounterH->Fill(5);
  return kTRUE;
}
//_____________________________________________________________________________
void AliAnalysisTaskNanoJPsi2018::UserExec(Option_t *)
{
  ////////////////////////////////////////////
  // general info of the event
  ////////////////////////////////////////////
  Int_t iSelectionCounter = 0; // no selection applied yet
  fCounterH->Fill(iSelectionCounter); // entering UserExec
  iSelectionCounter++;
  // get AOD event
  fAOD = dynamic_cast<AliAODEvent*>(InputEvent());
  if(!fAOD) {
    PostAllData();
    return;
  }
  fCounterH->Fill(iSelectionCounter); // AOD event found 2/2
  iSelectionCounter++;



  Bool_t isTriggered[3];
  isTriggered[0]=isTriggered[1]=isTriggered[2]=kFALSE;
  CheckTrigger(isTriggered);
  if (!isTriggered[0]&!isTriggered[1]&!isTriggered[2]) {
    PostAllData();
    return;
  }
  fRunNum = fAOD ->GetRunNumber();
    if (!fIsMC) {
    fTrgRunNum = fAOD->GetRunNumber();
    // Fill the trigger tree
    fTrgTree->Fill();
  }
  fCounterH->Fill(iSelectionCounter); // right trigger found
  iSelectionCounter++;
  // trigger inputs
  //fL0inputs = fAOD->GetHeader()->GetL0TriggerInputs();
  // if (fTrigger.Contains("CMUP6")){
  //   if(isTriggered[2]){
  //     fTriggerCounterFwdH->Fill(fRunNum);
  //     fCMUP6Decision = 1;
  //     fCMUP6 = 1;
  //   } else {
  //     fCMUP6Decision = 0;
  //     fCMUP6 = 0;
  //   }
  // }


  // get the run number


    ////////////////////////////////////////////
  //  MC generated particles analysis
  ////////////////////////////////////////////
  if(fIsMC){
    fGenPart->Clear("C");
    fMC = dynamic_cast<AliMCEvent*>(MCEvent());
    if(!fMC){
      PostAllData();
      return;
    }
    fCounterH->Fill(iSelectionCounter); // MC generated event found -/4
    iSelectionCounter++;

    //are there particles at all?
    Int_t nMCParticles(fMC->GetNumberOfTracks());
    if(nMCParticles<1) {
      PostAllData();
      return;
    }
    fCounterH->Fill(iSelectionCounter); // At least one MC generated particle -/5
    iSelectionCounter++;

    // loop over MC tracks and select muons
    Int_t nGoodMCPosMuons = 0;
    Int_t nGoodMCNegMuons = 0;
    Int_t *idxMCPosMuons = new Int_t[nMCParticles];
    Int_t *idxMCNegMuons = new Int_t[nMCParticles];
    for(Int_t iMCParticle = 0; iMCParticle < nMCParticles; iMCParticle++) {
      // get track
      AliMCParticle *MCPart = static_cast<AliMCParticle*>(fMC->GetTrack(iMCParticle));
      if(!MCPart) return;
      // Particle is primary (for LHC16b2) or is not primary and it is coming from J/Psi or Psi' decay (for LHC18l7)
      if(MCPart->GetMother() == -1){
        // if muons increase counter and store indices
        if(MCPart->PdgCode() == 13){
          idxMCPosMuons[nGoodMCPosMuons] = iMCParticle;
          nGoodMCPosMuons++;
        } else  if(MCPart->PdgCode() == -13){
          idxMCNegMuons[nGoodMCNegMuons] = iMCParticle;
          nGoodMCNegMuons++;
        }
      } else {
        AliMCParticle *MCMother = static_cast<AliMCParticle*>(fMC->GetTrack(MCPart->GetMother()));
        if(MCMother->PdgCode() != 443 && MCMother->PdgCode() != 100443) continue;
        // if muons increase counter and store indices
        if(MCPart->PdgCode() == 13){
          idxMCPosMuons[nGoodMCPosMuons] = iMCParticle;
          nGoodMCPosMuons++;
        } else  if(MCPart->PdgCode() == -13){
          idxMCNegMuons[nGoodMCNegMuons] = iMCParticle;
          nGoodMCNegMuons++;
        }
      }
    }
    // store number of muons
    fNumberMCMuonsH->Fill(nGoodMCPosMuons,nGoodMCNegMuons);

    ////////////////////////////////////////////
    // two MC muon analysis
    ////////////////////////////////////////////
    if (!(nGoodMCPosMuons == 1 && nGoodMCNegMuons == 1)) {
      PostAllData();
      return;
    }
    fCounterH->Fill(iSelectionCounter); // exactly one positive and one negative MC generated muons -/6
    iSelectionCounter++;
    TwoMCMuonAna(idxMCPosMuons,idxMCNegMuons);
    // FIll the MC generated tree
    fGenTree->Fill();
    }
   // end of MC generated particles


  ////////////////////////////////////////////
  //  find muons
  ////////////////////////////////////////////
  //are there tracks at all?
  Int_t nTracks(fAOD->GetNumberOfTracks());
  if(nTracks<1) {
    PostAllData();
    return;
  }
  fCounterH->Fill(iSelectionCounter); // At least one track 5/8
  iSelectionCounter++;

  // loop over tracks and select good muons
  Int_t nGoodPosMuons = 0;
  Int_t nGoodNegMuons = 0;
  Int_t *idxPosMuons = new Int_t[nTracks];
  Int_t *idxNegMuons = new Int_t[nTracks];
  for(Int_t iTrack = 0; iTrack < nTracks; iTrack++) {
    // get track
    AliAODTrack* track = static_cast<AliAODTrack*>(fAOD->GetTrack(iTrack));
    if(!track) return;

    // is it a good muon track?
    if(!track->IsMuonTrack()) continue;
    if(!fMuonTrackCuts->IsSelected(track)) continue;
    if( (track->GetRAtAbsorberEnd() < 17.5) || (track->GetRAtAbsorberEnd() > 89.5) ) continue;

    // increase counter and store indices
    if (track->Charge() > 0) {
      idxPosMuons[nGoodPosMuons] = iTrack;
      nGoodPosMuons++;
    } else  if (track->Charge() < 0) {
      idxNegMuons[nGoodNegMuons] = iTrack;
      nGoodNegMuons++;
    }
  }
  // store number of muons
  fNumberMuonsH->Fill(nGoodPosMuons,nGoodNegMuons);

  ////////////////////////////////////////////
  // two muon analysis
  ////////////////////////////////////////////
  if (!(nGoodPosMuons == 1 && nGoodNegMuons == 1)) {
    PostAllData();
    return;
  }
  fCounterH->Fill(iSelectionCounter); // exactly one positive and one negative muons 6/9
  iSelectionCounter++;
  TwoMuonAna(idxPosMuons,idxNegMuons);

  ////////////////////////////////////////////
  // info to determine exclusivity
  ////////////////////////////////////////////
  //  SPD
  fTracklets = fAOD->GetTracklets()->GetNumberOfTracklets();

  //  ZDC
  AliAODZDC *dataZDC = dynamic_cast<AliAODZDC*>(fAOD->GetZDCData());
  if(!dataZDC) {
    PostAllData();
    return;
  }
  fCounterH->Fill(iSelectionCounter); // ZDC info is present
  iSelectionCounter++;
  fZNAfired = dataZDC->IsZNAfired();
  fZNCfired = dataZDC->IsZNCfired();
  fZNAEnergy = dataZDC->GetZNATowerEnergy()[0];
  fZNCEnergy = dataZDC->GetZNCTowerEnergy()[0];
  fZPAEnergy = dataZDC->GetZPATowerEnergy()[0];
  fZPCEnergy = dataZDC->GetZPCTowerEnergy()[0];
  for (Int_t i=0;i<4;i++) fZNATDC[i] = dataZDC->GetZNATDCm(i);
  for (Int_t i=0;i<4;i++) fZNCTDC[i] = dataZDC->GetZNCTDCm(i);
  for (Int_t i=0;i<4;i++) fZPATDC[i] = dataZDC->GetZPATDCm(i);
  for (Int_t i=0;i<4;i++) fZPCTDC[i] = dataZDC->GetZPCTDCm(i);

  // V0
  AliVVZERO *dataVZERO = dynamic_cast<AliVVZERO*>(fAOD->GetVZEROData());
  if(!dataVZERO) {
     PostAllData();
    return;
  }

  fV0ADecision = dataVZERO->GetV0ADecision();
  fV0CDecision = dataVZERO->GetV0CDecision();
  fV0ATime = dataVZERO->GetV0ATime();
  fV0CTime = dataVZERO->GetV0CTime();
  for(Int_t i=0;i<32;i++) {
    fV0AMultiplicity[i] = dataVZERO->GetMultiplicityV0A(i);
    fV0CMultiplicity[i] = dataVZERO->GetMultiplicityV0C(i);
  }
  if(fV0ADecision == 0 && (fV0CDecision == 0 || fV0CDecision == 1) && fV0CFiredCells < 3){
  fCounterH->Fill(iSelectionCounter); //  V0 info
      iSelectionCounter++;

  }

  // ---AD
  AliVAD *dataAD = dynamic_cast<AliVAD*>(fAOD->GetADData());
  if(!dataAD){
    PostAllData();
    return;
  }
  fCounterH->Fill(iSelectionCounter); //  AD info 9/12
  iSelectionCounter++;

  fADADecision = dataAD->GetADADecision();
  fADCDecision = dataAD->GetADCDecision();

  // //Past-future protection maps
  // fIR1Map = fAOD->GetHeader()->GetIRInt1InteractionMap();
  // fIR2Map = fAOD->GetHeader()->GetIRInt2InteractionMap();

  // Fill the reconstruction tree
  fRecTree->Fill();

  // post the data
  PostAllData();

  // clean up
  delete [] idxPosMuons;
  delete [] idxNegMuons;

}
// ----------------------------------------------------------------------------------------------------------------------------------
void AliAnalysisTaskNanoJPsi2018::Terminate(Option_t *)
{
    cout << endl;
    // terminate
    // called at the END of the analysis (when all events are processed)
}
// ----------------------------------------------------------------------------------------------------------------------------------
