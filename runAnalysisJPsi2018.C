#include <sstream>
#include <vector>
#include <fstream>

//
// to run:
//   aliroot runNanoJPsi2018AnalysisFwd.C\(period\)


// include the header of your analysis task here!
#include "AliAnalysisTaskNanoJPsi2018.h"


void runAnalysisJPsi2018(Int_t period)
{
  // Bool_t local = kTRUE;
 Bool_t local = kFALSE;
  // if you run on grid, specify test mode (kTRUE) or full grid model (kFALSE)
  Bool_t gridTest = kFALSE;

    // since we will compile a class, tell root where to look for headers
#if !defined (__CINT__) || defined (__CLING__)
    gInterpreter->ProcessLine(".include $ROOTSYS/include");
    gInterpreter->ProcessLine(".include $ALICE_ROOT/include");
#else
    gROOT->ProcessLine(".include $ROOTSYS/include");
    gROOT->ProcessLine(".include $ALICE_ROOT/include");
#endif

    // create the analysis manager
    AliAnalysisManager *mgr = new AliAnalysisManager("AnalysisTaskNanoJPsi2018");
    AliAODInputHandler *aodH = new AliAODInputHandler();
    mgr->SetInputEventHandler(aodH);

    // compile the class and load the add task macro
    // here we have to differentiate between using the just-in-time compiler
    // from root6, or the interpreter of root5
#if !defined (__CINT__) || defined (__CLING__)
    gInterpreter->LoadMacro("AliAnalysisTaskNanoJPsi2018.cxx++g");
    char txt_cmd[120];
    sprintf(txt_cmd,"AddNanoJPsi2018.C(%d)",period);
    AliAnalysisTaskNanoJPsi2018 *task = reinterpret_cast<AliAnalysisTaskNanoJPsi2018*>(gInterpreter->ExecuteMacro(txt_cmd));
#else
    gROOT->LoadMacro("AliAnalysisTaskNanoJPsi2018.cxx++g");
    gROOT->LoadMacro("AddNanoJPsi2018.C");
    AliAnalysisTaskNanoJPsi2018 *task = AddNanoJPsi2018(period);
#endif
    // task->SetTrigger("CMUP26");
    task->AddTrigger("CMUP6");
    task->AddTrigger("CMUP26");
    task->AddTrigger("CMUP10");
    task->AddTrigger("CMUP13");
    task->AddTrigger("CMUP11");
    task->SetMinPtMuon(0);
    if(!mgr->InitAnalysis()) return;
    // mgr->SetDebugLevel(2);
    // mgr->PrintStatus();
    // mgr->SetUseProgressBar(1, 25);
    char tmpstr[120];
    if(local) {
        // if you want to run locally, we need to define some input
        TChain* chain = new TChain("aodTree");
        // add a few files to the chain (change this so that your local files are added)
		chain->Add("AliAOD_pass1_UD_266076_1.root");
		chain->Add("AliAOD_pass1_UD_266076_2.root");
		chain->Add("AliAOD_pass1_UD_266076_3.root");
		// start the analysis locally, reading the events from the tchain
		mgr->StartAnalysis("local", chain);
    } else {
      // if we want to run on grid, we create and configure the plugin
      AliAnalysisAlien *alienHandler = new AliAnalysisAlien();
      // also specify the include (header) paths on grid
      alienHandler->AddIncludePath("-I. -I$ROOTSYS/include -I$ALICE_ROOT -I$ALICE_ROOT/include -I$ALICE_PHYSICS/include");
      // make sure your source files get copied to grid
      alienHandler->SetAdditionalLibs("AliAnalysisTaskNanoJPsi2018.cxx AliAnalysisTaskNanoJPsi2018.h");
      alienHandler->SetAnalysisSource("AliAnalysisTaskNanoJPsi2018.cxx");
      // select the aliphysics version. all other packages
      // are LOADED AUTOMATICALLY!
      alienHandler->SetAliPhysicsVersion("vAN-20200422_ROOT6-1");
      // set the Alien API version
      alienHandler->SetAPIVersion("V1.1x");
      // select the input data
      if (period==0) {
	alienHandler->SetGridDataDir("/alice/data/2018/LHC18q");
	alienHandler->SetDataPattern("*muon_calo_pass3/AOD225/PWGUD/UD_PbPb_AOD/501_20190723-1440/*AliAOD.UPCNano.root");
	// MC has no prefix, data has prefix 000
	alienHandler->SetRunPrefix("000");
	// runnumber
	alienHandler->AddRunNumber(296510);
	// working dir
	alienHandler->SetGridWorkingDir("LHC18q_28_Oct_1");
	alienHandler->SetExecutable("LHC18q_Task.sh");
	alienHandler->SetJDLName("LHC18q_Task.jdl");
	std::cout<<"SetJDLNAAME"<<std::endl;
      } else if (period==1) {
	alienHandler->SetGridDataDir("/alice/data/2018/LHC18r");
	// alienHandler->SetDataPattern("*muon_calo_pass2/PWGUD/UD_PbPb_AOD/426_20190111-1316/*AliAOD.UPCNano.root");
	alienHandler->SetDataPattern("*muon_calo_pass3/AOD225/PWGUD/UD_PbPb_AOD/502_20190723-1441/*AliAOD.UPCNano.root");
	// MC has no prefix, data has prefix 000
	alienHandler->SetRunPrefix("000");
	// runnumber
	alienHandler->AddRunNumber(297481);
	// working dir
	alienHandler->SetGridWorkingDir("LHC18r_Oct_1");
	alienHandler->SetExecutable("LHC18r_Task.sh");
	alienHandler->SetJDLName("LHC18r_Task.jdl");
      }else if(period==2){
	alienHandler->SetGridDataDir("/alice/data/2015/LHC15o");
	alienHandler->SetDataPattern("*muon_calo_pass1/AOD229/PWGUD/UD_PbPb_AOD/522_20190828-1630/*AliAOD.UPCNano.root");
	// MC has no prefix, data has prefix 000
	alienHandler->SetRunPrefix("000");
	// runnumber
	alienHandler->AddRunNumber(246087);
	// working dir
	alienHandler->SetGridWorkingDir("LHC15o_Oct_1");
	alienHandler->SetExecutable("LHC15o_Task.sh");
	alienHandler->SetJDLName("LHC15o_Task.jdl");
      }
      // working dir
      else {
	cout << " not a valid option ... bye!" << endl;
	return;
      }

      // number of files per subjob
      alienHandler->SetSplitMaxInputFileNumber(20);
      // specify how many seconds your job may take
      alienHandler->SetTTL(10000);
      alienHandler->SetOutputToRunNo(kTRUE);
      alienHandler->SetKeepLogs(kTRUE);
      alienHandler->SetMaxMergeStages(1);
      alienHandler->SetMergeViaJDL(kTRUE);
      // define the output folders
      alienHandler->SetGridOutputDir("myOutputDir");
      // connect the alien plugin to the manager
      mgr->SetGridHandler(alienHandler);

      if(gridTest) {
	// speficy on how many files you want to run
	alienHandler->SetNtestFiles(1);
	// and launch the analysis
	alienHandler->SetRunMode("test");
	mgr->StartAnalysis("grid");
      } else {
	// else launch the full grid analysis
	//alienHandler->SetRunMode("full");
	alienHandler->SetRunMode("terminate");
	mgr->StartAnalysis("grid");
      }
    }
}
