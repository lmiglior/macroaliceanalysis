void plot1D(const char *fdatapath = "LHC18qr_Data", const char *fmcpath = "LHC18l7_JPsi_Incoh",
	  Double_t mumuPtmin = 0.35, Double_t mumuPtmax = 1.35, Double_t muPtmin = 0.0,
	  Int_t extractOpt = 0,
	  Int_t iteration = 0,
	  Int_t n1 = 0, Int_t n2 = -1,
		Bool_t reweightTrig = kFALSE)
{
  TFile *f = TFile::Open("results.root");
  TList *l = (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_%d_%d_%d_%d_%d",
				  fdatapath,fmcpath,
				  mumuPtmin,mumuPtmax,muPtmin,
				  extractOpt,iteration,n1,n2,reweightTrig));

  const char *hnames[3] = {"Theta","Phi","Tilda"};
  const char *varLabels[3] = {"cos#theta","#varphi","#varphi^{~}"};
  TCanvas *c1 = new TCanvas("c1","",1600,1000);
  c1->Divide(3,3);
  for(Int_t ivar = 0; ivar < 3; ++ivar) {
    TH1D *hData = (TH1D *)l->FindObject(Form("h%sData_Jpsi",hnames[ivar]));
    hData->SetStats(0);
    hData->SetLineWidth(2);
    hData->GetXaxis()->SetTitle(varLabels[ivar]);
    hData->GetXaxis()->SetLabelSize(0.05);
    hData->GetXaxis()->SetTitleSize(0.05);
    c1->cd(1+ivar);
    hData->Draw();
    TH1D *hAcc = (TH1D *)l->FindObject(Form("hAcc%s2",hnames[ivar]));
    hAcc->SetStats(0);
    hAcc->SetLineWidth(2);
    hAcc->GetXaxis()->SetTitle(varLabels[ivar]);
    hAcc->GetXaxis()->SetLabelSize(0.05);
    hAcc->GetXaxis()->SetTitleSize(0.05);
    c1->cd(4+ivar);
    hAcc->Draw();
    TH1D *hCorr = (TH1D *)l->FindObject(Form("h%sData_Jpsi_corrWoUnf",hnames[ivar]));
    hCorr->SetStats(0);
    hCorr->SetLineWidth(2);
    hCorr->GetXaxis()->SetTitle(varLabels[ivar]);
    hCorr->GetXaxis()->SetLabelSize(0.05);
    hCorr->GetXaxis()->SetTitleSize(0.05);
    c1->cd(7+ivar);
    hCorr->Draw();
    TF1 *fFit = (TF1 *)hCorr->GetListOfFunctions()->FindObject(Form("f%s",hnames[ivar]));
    TLatex *l1 = new TLatex;
    l1->DrawLatexNDC(0.5,0.7,Form("p%d = %.3f #pm %.3f",ivar+1,fFit->GetParameter(1),fFit->GetParError(1)));
    l1->DrawLatexNDC(0.5,0.6,Form("#chi^{2}/ndf = %.1f / %d",fFit->GetChisquare(),fFit->GetNDF()));
  }
}
