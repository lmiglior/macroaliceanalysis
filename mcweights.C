void mcweights(const char *fdatapath = "LHC18qr_Data", const char *fmcpath = "LHC18l7_JPsi_Incoh",
	       Double_t mumuPtmin = 0.35, Double_t mumuPtmax = 1.35, Double_t muPtmin = 0.0,
	       Int_t extractOpt = 0,
	       Int_t iteration = 0,
	       Int_t n1 = 0, Int_t n2 = -1,
	       Bool_t reweightTrig = kFALSE,
				 const char *typeMCfit = "cc", Int_t check = 0 )
{
  TFile *f;
  if (check==0) f = TFile::Open("results.root");
  else f = TFile::Open("results_newcheck.root");
  TList *l = (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_%d_%d_%d_%d_%d_cc",
				  fdatapath,fmcpath,
				  mumuPtmin,mumuPtmax,muPtmin,
				  extractOpt,iteration,n1,n2,reweightTrig));

  TCanvas *c1 = new TCanvas("c1","c1",1000,500);
  c1->Divide(2,1);
  TCanvas *c2 = new TCanvas("c2","",1000,500);
  c2->Divide(2,1);
  TH1D *hPtData = (TH1D *)l->FindObject("hPtData_Jpsi");
  hPtData->GetXaxis()->SetTitle("p_{T}^{rec}, GeV/c");
  hPtData->SetLineWidth(2);
  TH1D *hPtMC = (TH1D *)l->FindObject("hPtMC_Jpsi");
  hPtMC->GetXaxis()->SetTitle("p_{T}^{rec}, GeV/c");
  hPtMC->GetYaxis()->SetTitle("arb. units");
  hPtMC->SetLineColor(kRed);
  hPtMC->SetStats(0);
  hPtMC->SetLineWidth(2);
  c2->cd(1);
	TLatex *l1 = new TLatex;
  TLegend *leg = new TLegend(150,150);
  // leg->SetHeader("The Legend Title");
  leg->AddEntry(hPtMC,"MC","lpe");
  leg->AddEntry(hPtData,"Data","lpe");

  hPtMC->DrawNormalized("");
  hPtData->DrawNormalized("same");
  leg->Draw();
  TH1D *hPtRatio = (TH1D *)hPtData->Clone("hPtRatio");
  hPtRatio->Divide(hPtMC);
  TF1 *fPt = new TF1(Form("fPt_%s_%s_%.2f_%.2f_%.2f_%d_%d_%d_%d_%d_%s",
			  fdatapath,fmcpath,
			  mumuPtmin,mumuPtmax,muPtmin,
			  extractOpt,iteration,n1,n2,reweightTrig,typeMCfit),
		     "[1]*(1.0+[2]*(x-[0]))*TMath::Exp([3]*(x-[0])*(x-[0]))",
		     mumuPtmin,mumuPtmax);
  //fPt->FixParameter(0,mumuPtmin);
  fPt->FixParameter(0,mumuPtmax);
  Double_t initRatio = hPtData->Integral(1,hPtData->GetNbinsX())/hPtMC->Integral(1,hPtMC->GetNbinsX());
  fPt->SetParameter(1,initRatio);
  fPt->SetParameter(2,0.0);
  fPt->SetParameter(3,0.0);


  c1->cd(1);
  hPtRatio->Fit(fPt,"R");
	l1->SetTextColor(2);
	l1->SetTextSize(0.05);
	l1->DrawLatexNDC(0.35,0.55*0.3,Form("#chi^{2}/ndf = %.1f / %d",fPt->GetChisquare(),fPt->GetNDF()));
  TH1D *hYData = (TH1D *)l->FindObject("hYData_Jpsi");
  hYData->GetXaxis()->SetTitle("y^{rec}");
  hYData->SetLineWidth(2);
  TH1D *hYMC = (TH1D *)l->FindObject("hYMC_Jpsi");
  hYMC->GetXaxis()->SetTitle("y^{rec}");
  hYMC->GetYaxis()->SetTitle("arb. units");
  hYMC->SetLineColor(kRed);
  hYMC->SetStats(0);
  hYMC->SetLineWidth(2);
  c2->cd(2);
  hYMC->DrawNormalized("");
  hYData->DrawNormalized("same");
  TLegend *leg2 = new TLegend(300,300);
  // leg->SetHeader("The Legend Title");
  leg2->AddEntry(hYMC,"MC","lpe");
  leg2->AddEntry(hYData,"Data","lpe");
  leg2->Draw();
  TH1D *hYRatio = (TH1D *)hYData->Clone("hYRatio");
  hYRatio->Divide(hYMC);
  TF1 *fY = new TF1(Form("fY_%s_%s_%.2f_%.2f_%.2f_%d_%d_%d_%d_%d_%s",
			  fdatapath,fmcpath,
			  mumuPtmin,mumuPtmax,muPtmin,
			  extractOpt,iteration,n1,n2,reweightTrig,typeMCfit),
		     "[1]*(1.0+[2]*(x-[0])+[3]*(x-[0])*(x-[0]))",
		     -4.0,-2.5);
  fY->FixParameter(0,-3.25);
  Double_t initRatio2 = hYData->Integral(1,hYData->GetNbinsX())/hYMC->Integral(1,hYMC->GetNbinsX());
  fY->SetParameter(1,initRatio2);
  fY->SetParameter(2,0.0);
  fY->SetParameter(3,0.0);

  c1->cd(2);
  hYRatio->Fit(fY,"R");

  l1->SetTextColor(2);
  l1->SetTextSize(0.05);
	//l1->DrawLatexNDC(0.35,0.55*0.3,Form("#chi^{2}/ndf = %.1f / %d",fY->GetChisquare(),fY->GetNDF()));
	//l1->DrawLatexNDC(0.35,0.55*0.3,Form("#chi^{2}/ndf = 12.9 / %d",fY->GetNDF()));
  printf("%f %f\n",initRatio,initRatio2);

  if(string(typeMCfit)=="cc"){
    TFile foutput("mcweights.root","UPDATE");
    fPt->FixParameter(1,1.0);
    fPt->Write();
    fY->FixParameter(1,1.0);
    fY->Write();
    foutput.Close();
  }else if(string(typeMCfit)=="uu"){
    TFile foutput("mcweights_uu.root","UPDATE");
    // for (int i = 1; i < 4; i++) {
    //   if (i==1){
    fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    fPt->FixParameter(2,fPt->GetParameter(2)+fPt->GetParError(2));
    fY->FixParameter(2,fY->GetParameter(2)+fY->GetParError(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }else if(string(typeMCfit)=="ll"){
    TFile foutput("mcweights_ll.root","UPDATE");
    // for (int i = 1; i < 4; i++) {
    //   if (i==1){
    fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2)-fPt->GetParError(2));
    fY->FixParameter(2,fY->GetParameter(2)-fY->GetParError(2));

    fPt->Write();
    fY->Write();
    foutput.Close();
  }else if (string(typeMCfit)=="lu") {
    TFile foutput("mcweights_lu.root","UPDATE");
		fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2)-fPt->GetParError(2));
    fY->FixParameter(2,fY->GetParameter(2)+fY->GetParError(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }else if(string(typeMCfit)=="ul") {
    TFile foutput("mcweights_ul.root","UPDATE");
		fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2)+fPt->GetParError(2));
    fY->FixParameter(2,fY->GetParameter(2)-fY->GetParError(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }else if(string(typeMCfit)=="cl") {
    TFile foutput("mcweights_cl.root","UPDATE");
    fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2));
    fY->FixParameter(2,fY->GetParameter(2)-fY->GetParError(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }
  else if(string(typeMCfit)=="cu") {
    TFile foutput("mcweights_cu.root","UPDATE");
    fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2));
    fY->FixParameter(2,fY->GetParameter(2)+fY->GetParError(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }
  else if(string(typeMCfit)=="lc") {
    TFile foutput("mcweights_lc.root","UPDATE");
    fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2)-fPt->GetParError(2));
    fY->FixParameter(2,fY->GetParameter(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }
  else if(string(typeMCfit)=="uc") {
    TFile foutput("mcweights_uc.root","UPDATE");
    fPt->FixParameter(1,1.0);
    fY->FixParameter(1,1.0);
    //     }else{
    fPt->FixParameter(2,fPt->GetParameter(2)+fPt->GetParError(2));
    fY->FixParameter(2,fY->GetParameter(2));
    fPt->Write();
    fY->Write();
    foutput.Close();
  }
  //   TLatex *l1 = new TLatex;
  //   c1->cd(1);
  //   l1->DrawLatexNDC(0.2,0.4,Form("p_{T}^{rec} Data/MC %f",initRatio));
  // ;
  //  c1->cd(2);
  //   l1->DrawLatexNDC(0.2,0.4,Form("y^{rec} Data/MC %f",initRatio2));
  // ;
}
