void plotMyFractionFitting(const char *hname,
			   TH1D *hThetaData, TH1D *hPhiData, TH1D *hTildaData,
			   TH2D *hThetaMC, TH2D *hPhiMC, TH2D *hTildaMC,
			   TList *l);

void plotMyFractionsType(TList *l, const char *hname, const char *typeData, const char *typeMC);

void plotFractionsErr(const char *fdatapath = "LHC18qr_Data", const char *fmcpath = "LHC18l7_JPsi_Incoh",
		     Double_t mumuPtmin = 0.35, Double_t mumuPtmax = 1.35, Double_t muPtmin = 0.0,
		     Int_t n1 = 0, Int_t n2 = -1,
		     Bool_t reweightTrig = kFALSE){

  TFile *f = TFile::Open("results.root");
    TList *l[9];
	for(Int_t i=0;i<9;i++){
		std::cout<<i<<std::endl;
		if(i<3)	 	l[i]= (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_i_4_%d_%d_%d",
           fdatapath,fmcpath,
           mumuPtmin,mumuPtmax,muPtmin,
           n1,n2,reweightTrig));
		if (i>2&&i<6) {
			Int j=i+10;
			l[i]= (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_j_4_%d_%d_%d",
	           fdatapath,fmcpath,
	           mumuPtmin,mumuPtmax,muPtmin,
	           n1,n2,reweightTrig));
		}else if (i>5){
			Int j=i+20;
			l[i]= (TList *)f->Get(Form("%s_%s_%.2f_%.2f_%.2f_j_4_%d_%d_%d",
			           fdatapath,fmcpath,
			           mumuPtmin,mumuPtmax,muPtmin,
			           n1,n2,reweightTrig));
		}
	}


		plotMyFractionsType(l0,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l1,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l2,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l10,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l11,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l12,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l20,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l21,"MyData","Data_Jpsi","MC2");
		plotMyFractionsType(l22,"MyData","Data_Jpsi","MC2");

}

void plotMyFractionsType(TList *l, const char *hname, const char *typeData, const char *typeMC)
{
  TH1D *hTheta = (TH1D *)l->FindObject(Form("hTheta%s",typeData));
  TH1D *hPhi = (TH1D *)l->FindObject(Form("hPhi%s",typeData));
  TH1D *hTilda = (TH1D *)l->FindObject(Form("hTilda%s",typeData));

  TH2D *hDecompTheta = (TH2D *)l->FindObject(Form("hDecompTheta%s",typeMC));
  TH2D *hDecompPhi = (TH2D *)l->FindObject(Form("hDecompPhi%s",typeMC));
  TH2D *hDecompTilda = (TH2D *)l->FindObject(Form("hDecompTilda%s",typeMC));

  plotMyFractionFitting(hname,
			hTheta,hPhi,hTilda,
			hDecompTheta,hDecompPhi,hDecompTilda,
			l);
}

void plotMyFractionFitting(const char *hname,
		       TH1D *hThetaData, TH1D *hPhiData, TH1D *hTildaData,
		       TH2D *hThetaMC, TH2D *hPhiMC, TH2D *hTildaMC,
		       TList *l)
{
	TCanvas *c1 = new TCanvas(Form("%s_c1",hname),"",1500,1500);
	c1->Divide(3,1);
	c1->cd(1);
	TLegend *leg = new TLegend(0.65,0.75,0.9,0.9);
	hThetaData->SetStats(0);
	hThetaData->SetLineWidth(2);
	hThetaData->GetXaxis()->SetTitle("cos#theta");
	hThetaData->GetYaxis()->SetRangeUser(0,hThetaData->GetMaximum()*1.1);
	hThetaData->Draw();
	c1->cd(2);
	hPhiData->SetStats(0);
	hPhiData->SetLineWidth(2);
	hPhiData->GetXaxis()->SetTitle("#varphi, rad");
	hPhiData->GetYaxis()->SetRangeUser(0,hThetaData->GetMaximum()*1.1);
	hPhiData->Draw();
	c1->cd(3);
	hTildaData->SetStats(0);
	hTildaData->SetLineWidth(2);
	hTildaData->GetXaxis()->SetTitle("#tilde{#varphi}, rad");
	hTildaData->GetYaxis()->SetRangeUser(0,hThetaData->GetMaximum()*1.1);
	hTildaData->Draw();
	hTildaMCscaled[3]->SetLineColor(kYellow-2);
	hTildaMCscaled[3]->Draw("hist same");


}
