/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. */
/* See cxx source for full Copyright notice */
/* $Id$ */

#ifndef AliAnalysisTaskNanoJPsi2018_H
#define AliAnalysisTaskNanoJPsi2018_H

#include <AliAnalysisTaskSE.h>

class AliMuonTrackCuts; 	// Include class for standard muon tack cuts


class AliAnalysisTaskNanoJPsi2018 : public AliAnalysisTaskSE
{
public:
  AliAnalysisTaskNanoJPsi2018();
  AliAnalysisTaskNanoJPsi2018(const char *name);
  virtual                 ~AliAnalysisTaskNanoJPsi2018();

  virtual void          UserCreateOutputObjects();
  virtual void          UserExec(Option_t* option);
  virtual void          Terminate(Option_t* option);
  virtual void   	NotifyRun();	 // Implement the Notify run to search for the new parameters at each new runs

  void			BuildRecDiMuons();			                        // Analyses muons and extracs dimuon information
  void 			BuildGenDiMuons();	  // Analyses two MC muons and extracs MC dimuon information
  void 			AddTrigger(const char *trigger){fTriggerToAnalyse[fNTriggers++] = trigger;}
  //  void                  SetTrigger(TString trigger){fTriggerToAnalyse = trigger;}
  void                  SetPeriod(Int_t period){fPeriod = period;}
  void                  SetMC(Bool_t flag){fIsMC = flag;}
  void 			PostAllData();
  void                  SetMinPtMuon(Double_t ptMinMuon) {fPtMinMuon = ptMinMuon;}

  Bool_t 		GoodMUONTrack(AliAODTrack* track);
  Bool_t                CheckTrigger();
  Bool_t                CheckV0AD();

  AliMuonTrackCuts* 	fMuonTrackCuts; 					// Use the class as a data member

private:
  TString 				fPeriod;
  TString 				fTriggerToAnalyse[10];
  Int_t                                 fNTriggers;
  //TString                               fTriggerToAnalyse;
  Bool_t 				fIsMC;
  Bool_t 				fIsScalingOn;
  Double_t                              fPtMinMuon;

  AliAODEvent*    fAOD;       		//! input event
  AliMCEvent*	  fMC;				//! input MC event

  TList*          fOutputList; 		//! output list
  TH1F*           fEvRecCounter; 			//! counter for events passing each cut
  TH1F*           fEvGenCounter; 			//! counter for events passing each cut
  TH1D*           fNumberRecMuons; 		//! count good muons per event
  TH1D*           fNumberGenMuons;	//! count MC muons per event
  TH1F*           fMuonTrackCounterH; //track counter
  TH2D*           fMuonMassPtGenRec; // (pt_gen - pt_rec) vs pt_gen


  TTree *fTree; //! analysis tree
  TTree *fTreeGen; //! gen analysis tree
  Int_t fRunNum;
  Double_t fMuMuPt;
  Double_t fMuMuPhi;
  Double_t fMuMuDeltaPhi;
  Double_t fMuMuY;
  Double_t fMuMuM;
  Double_t fMuPt1;
  Double_t fMuPt2;
  Int_t fCharge;
  Double_t fMuMuEta;
  Double_t fMuPhi1;
  Double_t fMuPhi2;
  Double_t fMuEta1;
  Double_t fMuEta2;
  Int_t fMuCharge1;
  Int_t fMuCharge2;
  Int_t fMCRunNum;
  Double_t fMCMuMuPt;
  Double_t fMCMuMuPhi;
  Double_t fMCMuMuDeltaPhi;
  Double_t fMCMuMuY;
  Double_t fMCMuMuM;
  Double_t fMCMuPt1;
  Double_t fMCMuPt2;
  Int_t fMCCharge;
  Int_t fMCMuCharge1;
  Int_t fMCMuCharge2;
  Double_t fMCMuMuEta;
  Double_t fMCMuPhi1;
  Double_t fMCMuPhi2;
  Double_t fMCMuEta1;
  Double_t fMCMuEta2;

  UInt_t fL0inputs;
  Int_t fTracklets;
  Float_t fZNCEnergy;
  Float_t fZNAEnergy;
  Float_t fZNATDC[4];
  Float_t fZNCTDC[4];
  Int_t fADCDecision;
  Int_t fADADecision;
  Int_t fV0FiredCells;
  Int_t fV0ADecision;
  Int_t fV0CDecision;
  Double_t fV0ATime;
  Double_t fV0CTime;
  Double_t fADATime;
  Double_t fADCTime;
  Double_t fPtMin;
  Int_t fNRecMuons;
  Int_t fIdxMuons[20]; //all index muon
  Int_t fNGenMuons; //mc muons
  Int_t fIdxMCMuons[20]; //all mc index muon
  THnSparseD *fDiMuonKinematicsRec[2][3]; //nSparse rec three dimension array [i][j] i=0 2 Dimuons, j=charge
  THnSparseD *fDiMuonKinematicsGen[2][3]; //nSparse gen three dimension array [i][j] i=0 2 Dimuons, j=charge
  TClonesArray *fGenPart;

  AliAnalysisTaskNanoJPsi2018(const AliAnalysisTaskNanoJPsi2018&); // not implemented
  AliAnalysisTaskNanoJPsi2018& operator=(const AliAnalysisTaskNanoJPsi2018&); // not implemented

  ClassDef(AliAnalysisTaskNanoJPsi2018, 1);
};

#endif
